# <Область автоматизации № 1>
Система планирования аукционов  
Эта система включает в себя набор методов, данных и связей между ними для организации корректно работающего аукциона в соотвествии с бизнес-логикой заказчика.  
Аукцион включает данные проведения: классификация по открытости, интерактивности и по тому, увеличивается или уменьшается ставка.  
Аукцион включает набор лотов, содержащих информацию о начальной ставке, кванте времени для изменения цены, предельной цене, статусе и др.  
Классы содержат все необходимые методы для операций над ними.  