import unittest
from .PreparedStorage import *
from ..UseCases.DeclineRequestToInteractiveAuction import *


class TestDeclineRequestToInteractiveAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = DeclineRequestToInteractiveAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        decline_lot_id = 0
        # check usecase with normal auction_id and request_lot_id
        lot_id = self.use_case.run(1, decline_lot_id)
        self.assertEqual(lot_id, decline_lot_id)
        declined_lot = self.use_case.storage.get_request_lot(1, decline_lot_id)
        self.assertEqual(declined_lot.get_status(), "declined")
        # check usecase with wrong request_lot status
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, decline_lot_id)
        self.assertEqual(exception_msg.exception.args[0], "Wrong lot status")
        # check usecase with wrong auction_id
        wrong_auction_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(wrong_auction_id, 1)
        self.assertEqual(exception_msg.exception.args[0], "Not find auction with this id")
        # check usecase with wrong request_lot_id
        wrong_lot_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, wrong_lot_id)
        self.assertEqual(exception_msg.exception.args[0], "Not find request lot with this id")


if __name__ == '__main__':
    unittest.main()
