import unittest
from .PreparedStorage import *
from ..UseCases.ApproveRequestToInteractiveAuction import *


class TestApproveRequestToInteractiveAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = ApproveRequestToInteractiveAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_lot_0 = Lot(0, 10.0, "car_1", 1, 20, 10.0, 100000.0)
        new_lot_1 = Lot(4, 50.0, "car_5", 1, 20, 10.0, 100000.0)
        new_lot_2 = Lot(5, 50.0, "car_6", 1, 20, 10.0, 100000.0)
        auction = self.use_case.storage.get_interactive_auction(1)
        self.use_case.storage.add_request_lot(1, new_lot_1)
        self.use_case.storage.add_request_lot(1, new_lot_2)
        # check usecase with normal auction_id and request_lot_id
        lot_id = self.use_case.run(1, new_lot_0.id)
        self.assertEqual(lot_id, new_lot_0.id)
        self.assertEqual(auction.schedule[-1].id, new_lot_0.id)
        self.assertEqual(auction.schedule[-1].get_status(), "approved")
        self.assertEqual(self.use_case.storage.get_request_lot(1, new_lot_0.id), None)
        # check usecase with last request_lot for auction
        lot_id = self.use_case.run(1, new_lot_1.id)
        self.assertEqual(lot_id, new_lot_1.id)
        self.assertEqual(auction.schedule[-1].id, new_lot_1.id)
        self.assertEqual(auction.schedule[-1].get_status(), "approved")
        self.assertEqual(self.use_case.storage.get_request_lot(1, new_lot_2.id).get_status(), "declined")
        # check usecase with wrong auction_id
        wrong_auction_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(wrong_auction_id, 1)
        self.assertEqual(exception_msg.exception.args[0], "Not find auction with this id")
        # check usecase with wrong request_lot_id
        wrong_lot_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, wrong_lot_id)
        self.assertEqual(exception_msg.exception.args[0], "Not find request lot with this id")
        # check usecase with wrong request_lot status
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, new_lot_2.id)
        self.assertEqual(exception_msg.exception.args[0], "Wrong lot status")
        # check usecase with existing lot
        existing_lot = Lot(0, 10.0, "car_1", 1, 20, 10.0, 100000.0)
        self.use_case.storage.add_request_lot(1, existing_lot)
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, existing_lot.id)
        self.assertEqual(exception_msg.exception.args[0], "Lot with this id exist")


if __name__ == '__main__':
    unittest.main()
