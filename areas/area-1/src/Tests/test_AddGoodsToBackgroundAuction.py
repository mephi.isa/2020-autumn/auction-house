import unittest
from .PreparedStorage import *
from ..UseCases.AddGoodsToBackgroundAuction import *


class TestAddGoodsToBackgroundAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = AddGoodsToBackgroundAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_lot_data = LotInputData(4, 50.0, "car_5", 1, 20, 10.0, 100000.0)
        background_auction = self.use_case.storage.get_background_auction()
        # check usecase with normal lot
        lot_id = self.use_case.run(new_lot_data)
        self.assertEqual(lot_id, new_lot_data.id)
        self.assertEqual(background_auction.schedule[-1].id, new_lot_data.id)
        self.assertEqual(background_auction.schedule[-1].get_status(), "approved")
        # check usecase with existing lot
        existing_lot_data = new_lot_data
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(existing_lot_data)
        self.assertEqual(exception_msg.exception.args[0], "Lot with this id exist")


if __name__ == '__main__':
    unittest.main()
