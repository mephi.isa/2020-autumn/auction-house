import unittest
from .PreparedStorage import *
from ..UseCases.ListAuctions import *


class TestListAuctions(unittest.TestCase):
    storage = PreparedStorage()
    use_case = ListAuctions(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        auction_list = self.use_case.storage.get_all_interactive_auctions()
        auction_data_list = self.use_case.run()
        for i, auction in enumerate(auction_data_list):
            self.assertEqual(auction_data_list[i].id, auction_list[i].id)


if __name__ == '__main__':
    unittest.main()
