from ..Classes.auction_planning import *
import unittest
from datetime import *

auction_test_date = datetime(2021, 1, 1, 9, 0, 0)
bad_auction_test_date = datetime(2020, 1, 1, 9, 0, 0)

test_lot1 = Lot(1, 2000.0, "lot", 1, 30, 100.0, 1000.0)
test_lot2 = Lot(2, 3000.0, "lot", 1, 30, 100.0, 2000.0)

class test_auction(unittest.TestCase):

    def test_init_auction_with_bad_types(self):
        self.assertRaises(TypeError, Auction, "a", "", "", "", "", "", "","", "")
        self.assertRaises(TypeError, Auction, 1, "", "", "", "", "", "","", "")
        self.assertRaises(ValueError, Auction, 1, -1, "", "", "", "", "","", "")
        self.assertRaises(TypeError, Auction, 1, 1, "", "", True, True, "","", "")
        self.assertRaises(TypeError, Auction, 1, 1, "", True, "", True, "","", "")
        self.assertRaises(TypeError, Auction, 1, 1, "", True, True, "", "","", "")
        self.assertRaises(TypeError, Auction, 1, 1, "", True, True, bad_auction_test_date, "","", "")

    def test_approve_auction(self):
        auction = Auction(1, 1, "default", True, True, True, auction_test_date, 100, 1)
        assert auction.is_approved == False
        auction.approve()   
        assert auction.is_approved == True
        self.assertRaises(ValueError, auction.set_cap, 2)
        self.assertRaises(ValueError, auction.set_is_incr, True)
        self.assertRaises(ValueError, auction.set_is_interactive, True)
        self.assertRaises(ValueError, auction.set_is_opened, True)
        self.assertRaises(ValueError, auction.set_start_datetime, True)

    def test_add_lot(self):
        auction = Auction(1, 1, "default", True, True, True, auction_test_date, 100, 1)
        self.assertRaises(ValueError, auction.add_lot, test_lot1)
        auction.approve() 

        self.assertRaises(TypeError, auction.add_lot, 1)

        auction.add_lot(test_lot1)
        assert len(auction.schedule) == 1
        assert auction.schedule[0] == test_lot1

        self.assertRaises(ValueError, auction.add_lot, test_lot2)

    def test_delete_lot(self):
        auction = Auction(1, 2, "default", True, True, True, auction_test_date, 100, 1)
        auction.approve()
        auction.add_lot(test_lot1)
        auction.add_lot(test_lot2)

        auction.delete_lot(1)
        assert len(auction.schedule) == 1
        assert auction.schedule[0] == test_lot2

        auction.delete_lot(3)
        assert len(auction.schedule) == 1
        assert auction.schedule[0] == test_lot2

class test_lot(unittest.TestCase):
    def test_init_lot_with_bad_types(self):
        self.assertRaises(TypeError, Lot, "", 2000.0, "lot", 1, 30, 100.0, 1000.0)
        self.assertRaises(TypeError, Lot, 1, "", "lot", 1, 30, 100.0, 1000.0)
        self.assertRaises(ValueError, Lot, 1, -2000.0, "lot", 1, 30, 100.0, 1000.0)
        self.assertRaises(ValueError, Lot, 1, 2000.0, "lot", -1, 30, 100.0, 1000.0)
        self.assertRaises(TypeError, Lot, 1, 2000.0, "lot", 1, "", 100.0, 1000.0)
        self.assertRaises(ValueError, Lot, 1, 2000.0, "lot", 1, -30, 100.0, 1000.0)
        self.assertRaises(TypeError, Lot, 1, 2000.0, "lot", 1, 30, "", 1000.0)
        self.assertRaises(ValueError, Lot, 1, 2000.0, "lot", 1, 30, -100.0, 1000.0)
        self.assertRaises(TypeError, Lot, 1, 2000.0, "lot", 1, 30, 100.0, "")
        self.assertRaises(ValueError, Lot, 1, 2000.0, "lot", 1, 30, 100.0, -1000.0)

    def test_lot_status(self):
        lot = Lot(1, 2000.0, "lot", 1, 30, 100.0, 1000.0)
        lot.approve()
        assert lot.get_status() == "approved"
        lot.decline()
        assert lot.get_status() == "declined"


if __name__ == '__main__':
    unittest.main()