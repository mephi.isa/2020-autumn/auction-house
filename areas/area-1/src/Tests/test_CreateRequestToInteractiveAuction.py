import unittest
from .PreparedStorage import *
from ..UseCases.CreateRequestToInteractiveAuction import *


class CreateRequestToInteractiveAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = CreateRequestToInteractiveAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_lot_data = LotInputData(4, 50.0, "car_5", 1, 20, 10.0, 100000.0)
        # check usecase with normal auction_id and lot_data
        lot_id = self.use_case.run(1, new_lot_data)
        self.assertEqual(lot_id, new_lot_data.id)
        lot = self.use_case.storage.get_request_lot(1, lot_id)
        self.assertEqual(lot.id, new_lot_data.id)
        self.assertEqual(lot.description, new_lot_data.description)
        self.assertEqual(lot.get_status(), "opened")
        # check usecase with existing lot_data
        existing_lot_data = new_lot_data
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(1, existing_lot_data)
        self.assertEqual(exception_msg.exception.args[0], "Request lot with this id exist")
        # check usecase with wrong auction_id
        wrong_auction_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(wrong_auction_id, existing_lot_data)
        self.assertEqual(exception_msg.exception.args[0], "Not find auction with this id")


if __name__ == '__main__':
    unittest.main()
