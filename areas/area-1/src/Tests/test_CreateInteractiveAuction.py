import unittest
from .PreparedStorage import *
from ..UseCases.CreateInteractiveAuction import *


class TestCreateInteractiveAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = CreateInteractiveAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_auction_data = InteractiveAuctionInputData(4, 30, "auction_4", True, True,
                            datetime(2020, 12, 17, 0, 0, 0), 1000, 4)
        # check usecase with normal auction_data
        auction_id = self.use_case.run(new_auction_data)
        self.assertEqual(auction_id, new_auction_data.id)
        auction = self.use_case.storage.get_interactive_auction(new_auction_data.id)
        self.assertEqual(auction.id, new_auction_data.id)
        self.assertEqual(auction.theme, new_auction_data.theme)
        self.assertEqual(auction.start_date, new_auction_data.start_datetime)
        self.assertEqual(auction.durability, new_auction_data.durability)
        # check usecase with existing auction_data
        existing_auction_data = new_auction_data
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(existing_auction_data)
            self.assertEqual(str(exception_msg), "Auction with this id exist")


if __name__ == '__main__':
    unittest.main()
