import unittest
from .PreparedStorage import *
from ..UseCases.StartAuction import *


class TestStartAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = StartAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_auction = Auction(4, 30, "auction_4", True, True, True, datetime(2020, 12, 17, 0, 0, 0), 1000, 3)
        new_auction.start_date = datetime.now()
        self.use_case.storage.add_interactive_auction(new_auction)
        start_auction_data_list = self.use_case.run()
        self.assertEqual(len(start_auction_data_list), 1)
        self.assertEqual(start_auction_data_list[0].id, new_auction.id)


if __name__ == '__main__':
    unittest.main()
