import unittest
from .PreparedStorage import *
from ..UseCases.ChangeBackgroundAuction import *


class TestChangeBackgroundAuction(unittest.TestCase):
    storage = PreparedStorage()
    use_case = ChangeBackgroundAuction(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        new_auction_data = BackgroundAuctionInputData(100)
        # check usecase with normal auction_data
        auction_id = self.use_case.run(new_auction_data)
        background_auction = self.use_case.storage.get_background_auction()
        self.assertEqual(background_auction.id, auction_id)
        self.assertEqual(background_auction.durability, new_auction_data.durability)


if __name__ == '__main__':
    unittest.main()
