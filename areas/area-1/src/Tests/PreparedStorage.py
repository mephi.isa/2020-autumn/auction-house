from datetime import datetime, timedelta
from ..Repository.Storage import *


class PreparedStorage(Storage):
    def __init__(self):
        super().__init__()
        self.lots = [Lot(0, 10.0, "car_1", 1, 20, 10.0, 100000.0),
                     Lot(1, 20.0, "car_2", 2, 20, 10.0, 100000.0),
                     Lot(2, 30.0, "car_3", 3, 20, 10.0, 100000.0),
                     Lot(3, 40.0, "car_4", 4, 20, 10.0, 100000.0)]
        self.lots[1].approve()
        self.lots[2].approve()
        self.lots[3].approve()
        self.request_lots = {1: [self.lots[0]]}
        today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        self.background_auction = Auction(0, 10000, "background_auction", True, True, False,
                                          today + timedelta(days=1000), 0, 1000)
        self.background_auction.approve()
        self.interactive_auctions = [
            Auction(1, 2, "auction_1", True, True, True,
                    today + timedelta(days=10), 1000, 1),
            Auction(2, 20, "auction_2", True, True, True,
                    today + timedelta(days=11), 1000, 2),
            Auction(3, 30, "auction_3", True, True, True,
                    today + timedelta(days=12), 1000, 3)
        ]
        self.interactive_auctions[0].approve()
        self.interactive_auctions[1].approve()
        self.interactive_auctions[2].approve()
        self.request_lots = {1: [self.lots[0]]}
        self.interactive_auctions[1].add_lot(self.lots[1])
        self.interactive_auctions[2].add_lot(self.lots[2])
        self.interactive_auctions[2].add_lot(self.lots[3])

    def get_interactive_auction(self, auction_id) -> Auction:
        for auction in self.interactive_auctions:
            if auction.id == auction_id:
                return auction

    def get_all_interactive_auctions(self) -> list:
        return self.interactive_auctions

    def update_interactive_auction(self, auction: Auction):
        for i, interactive_auction in enumerate(self.interactive_auctions):
            if interactive_auction.id == auction.id:
                self.interactive_auctions[i] = auction
                return auction.id

    def add_interactive_auction(self, auction: Auction) -> int:
        self.interactive_auctions.append(auction)
        return auction.id

    def get_background_auction(self) -> Auction:
        return self.background_auction

    def update_background_auction(self, auction: Auction) -> int:
        self.background_auction = auction
        return auction.id

    def add_request_lot(self, auction_id: int, lot: Lot) -> int:
        request_lots = self.request_lots.get(auction_id)
        if request_lots is None:
            self.request_lots[auction_id] = [lot]
        else:
            self.request_lots[auction_id].append(lot)
        return lot.id

    def get_request_lot(self, auction_id: int, lot_id: int) -> Lot:
        request_lots = self.request_lots.get(auction_id)
        if request_lots is not None:
            for request_lot in request_lots:
                if request_lot.id == lot_id:
                    return request_lot

    def update_request_lot(self, auction_id: int, lot: Lot) -> int:
        request_lots = self.request_lots.get(auction_id)
        if request_lots is not None:
            for i, request_lot in enumerate(request_lots):
                if request_lot.id == lot.id:
                    self.request_lots[auction_id][i] = lot
                    return lot.id

    def delete_request_lot(self, auction_id: int, lot_id: int) -> int:
        request_lots = self.request_lots.get(auction_id)
        if request_lots is not None:
            for i, request_lot in enumerate(request_lots):
                if request_lot.id == lot_id:
                    del self.request_lots[auction_id][i]
                    return lot_id

    def get_all_request_lots_to_auction(self, auction_id: int) -> list:
        request_lots = self.request_lots.get(auction_id)
        if request_lots is None:
            request_lots = []
        return request_lots


