import unittest
from .PreparedStorage import *
from ..UseCases.ListRequestLots import *


class TestListAuctions(unittest.TestCase):
    storage = PreparedStorage()
    use_case = ListRequestLots(storage)

    def test_constructor(self):
        self.assertEqual(self.use_case.storage, self.storage)

    def test_run(self):
        auction_id = 1
        lot_list = self.use_case.storage.get_all_request_lots_to_auction(auction_id)
        # check usecase with normal auction_id
        free_number_and_lot_data = self.use_case.run(auction_id)
        free_lots_number = free_number_and_lot_data[0]
        lot_data_list = free_number_and_lot_data[1]
        self.assertEqual(free_lots_number, 2)
        for i, lot in enumerate(lot_data_list):
            self.assertEqual(lot_data_list[i].id, lot_list[i].id)
        # check usecase with auction without request_lots
        no_requests_auction_id = 2
        free_number_and_lot_data = self.use_case.run(no_requests_auction_id)
        self.assertEqual(free_number_and_lot_data, [19, []])
        # check usecase with wrong auction_id
        wrong_auction_id = -1
        with self.assertRaises(RuntimeError) as exception_msg:
            self.use_case.run(wrong_auction_id)
        self.assertEqual(exception_msg.exception.args[0], "Not find auction with this id")


if __name__ == '__main__':
    unittest.main()
