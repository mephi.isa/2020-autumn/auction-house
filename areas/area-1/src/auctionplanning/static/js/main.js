
function Main() {
	const role = $('#select_role').val()
	

    $.ajax({
		url:    	'',
		method:		'PUT',
		cache: 		false,
        data:   	JSON.stringify({'role': role}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function AddGoodsToBackgroundAuction() {
	const lot_id = 0;
    const start_price = $('#start_price').val();
	const description = $('#description').val();
	const seller_id = 1;
    const quantum = $('#quantum').val();
    const price_change = $('#price_change').val();
    const limit_price = $('#limit_price').val();


    $.ajax({
		url:    	'add_goods_to_background_auction',
		method:		'POST',
		cache: 		false,
        data:   	JSON.stringify({'id': lot_id, 'start_price': start_price, 'description': description, 
        'seller_id': seller_id, 'quantum': quantum, 'price_change': price_change, 'limit_price': limit_price}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function CreateRequestToInteractiveAuction() {
	const auction_id = $('#auction_id').val();
	const lot_id = 0;
    const start_price = $('#start_price').val();
	const description = $('#description').val();
	const seller_id = 1;
    const quantum = $('#quantum').val();
    const price_change = $('#price_change').val();
    const limit_price = $('#limit_price').val();


    $.ajax({
		url:    	'create_request_to_interactive_auction',
		method:		'POST',
		cache: 		false,
        data:   	JSON.stringify({'auction_id': auction_id, 'id': lot_id, 'start_price': start_price, 
        'description': description, 'seller_id': seller_id, 'quantum': quantum, 'price_change': price_change, 
        'limit_price': limit_price}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function RequestForm() {
    const auction_id = $('#auction_id').val();


    $.ajax({
		url:    	'create_request_to_interactive_auction',
		method:		'PUT',
		cache: 		false,
        data:   	JSON.stringify({'auction_id': auction_id}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function ApproveRequestToInteractiveAuction() {
	const auction_id = $('#auction_id').val();
	const request_id = $('#request_id').val();


	$.ajax({
		url:    	'action_with_request_to_interactive_auction',
		method:		'POST',
		cache: 		false,
        data:   	JSON.stringify({'auction_id': auction_id, 'lot_id': request_id, 'action': 'approve'}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function DeclineRequestToInteractiveAuction() {
	const auction_id = $('#auction_id').val();
	const request_id = $('#request_id').val();


	$.ajax({
		url:    	'action_with_request_to_interactive_auction',
		method:		'POST',
		cache: 		false,
        data:   	JSON.stringify({'auction_id': auction_id, 'lot_id': request_id, 'action': 'decline'}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function CreateInteractiveAuction() {
    const auction_id = 0
    const cap = $('#cap').val();
    const theme = $('#theme').val();
    const is_opened = ($('#is_opened').val() == 'true');
    const is_incr = ($('#is_incr').val() == 'true');
	const start_datetime = $('#start_datetime').val();
	const durability = $('#durability').val();
	const employee_id = 1;
	

    $.ajax({
		url:    	'create_interactive_auction',
		method:		'POST',
		cache: 		false,
		data:   	JSON.stringify({'id': auction_id, 'cap': cap, 'theme': theme, 'is_opened': is_opened, 
		'is_incr': is_incr, 'start_datetime': start_datetime, 'durability': durability, 'employee_id': employee_id}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
}

function ListRequestLots(){
	const auction_id = $('#auction_id').val();


    $.ajax({
		url:    	'action_with_request_to_interactive_auction',
		method:		'PUT',
		cache: 		false,
        data:   	JSON.stringify({'auction_id': auction_id}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
 }

 function ChangeBackgroundAuction(){
	const durability = $('#durability').val();


    $.ajax({
		url:    	'change_background_auction',
		method:		'PUT',
		cache: 		false,
        data:   	JSON.stringify({'durability': durability}),
		contentType: 'application/json; charset=utf-8',
        dataType:	'html',
		success: function(data) {
			$('body').html(data);
		}
	});
 }
