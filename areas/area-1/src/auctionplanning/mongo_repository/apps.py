from django.apps import AppConfig


class MongoRepositoryConfig(AppConfig):
    name = 'mongo_repository'
