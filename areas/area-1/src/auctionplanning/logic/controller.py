from repository.storage import *
from data.BackgroundAuctionData import *
from data.InteractiveAuctionData import *
from data.LotData import *
from datetime import datetime, timedelta


class AuctionPlanningController:
    def __init__(self, db: Storage):
        self.storage = db

    def add_goods_to_background_auction(self, lot_data: LotInputData) -> int:
        lot = Lot(lot_data.id, lot_data.start_price, lot_data.description, lot_data.seller_id,
                  lot_data.quantum, lot_data.price_change, lot_data.limit_price)
        self.check_lot(lot.id)
        background_auction = self.storage.get_background_auction()
        lot.approve()
        background_auction.add_lot(lot)
        self.storage.update_background_auction(background_auction)
        return lot_data.id

    def approve_request_to_interactive_auction(self, auction_id: int, lot_id: int) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_exist(auction_id, lot_id)
        auction = self.storage.get_interactive_auction(auction_id)
        request_lot = self.storage.get_request_lot(auction_id, lot_id)
        self.check_request_lot_status(request_lot.get_status())
        self.check_auction_lot_not_exist(auction_id, lot_id)
        request_lot.approve()
        auction.add_lot(request_lot)
        del_lot_id = self.storage.delete_request_lot(auction_id, lot_id)
        self.storage.update_interactive_auction(auction)
        if len(auction.schedule) == auction.cap:
            all_request_lots = self.storage.get_all_request_lots_to_auction(auction_id)
            if all_request_lots is not None:
                for request_lot in all_request_lots:
                    request_lot.decline()
                    self.storage.update_request_lot(auction_id, request_lot)
        return del_lot_id

    def change_background_auction(self, background_auction_data: BackgroundAuctionInputData) -> int:
        background_auction = self.storage.get_background_auction()
        background_auction.durability = background_auction_data.durability
        auction_id = self.storage.update_background_auction(background_auction)
        return auction_id

    def create_interactive_auction(self, interactive_auction_data: InteractiveAuctionInputData) -> int:
        self.check_auction_not_exist(interactive_auction_data.id)
        auction = Auction(interactive_auction_data.id,
                          interactive_auction_data.cap,
                          interactive_auction_data.theme,
                          interactive_auction_data.is_opened,
                          interactive_auction_data.is_incr,
                          True,
                          interactive_auction_data.start_datetime,
                          interactive_auction_data.durability,
                          interactive_auction_data.employee_id)
        auction_id = self.storage.add_interactive_auction(auction)
        return auction_id

    def create_request_to_interactive_auction(self, auction_id: int, lot_data: LotInputData) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_not_exist(auction_id, lot_data.id)
        lot = Lot(lot_data.id, lot_data.start_price, lot_data.description, lot_data.seller_id,
                  lot_data.quantum, lot_data.price_change, lot_data.limit_price)
        lot_id = self.storage.add_request_lot(auction_id, lot)
        return lot_id

    def decline_request_to_interactive_auction(self, auction_id: int, lot_id: int) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_exist(auction_id, lot_id)
        request_lot = self.storage.get_request_lot(auction_id, lot_id)
        self.check_request_lot_status(request_lot.get_status())
        request_lot.decline()
        self.storage.update_request_lot(auction_id, request_lot)
        return request_lot.id

    def list_auctions(self) -> list:
        auction_data_list = []
        interactive_auction_list = self.storage.get_all_interactive_auctions()
        for auction in interactive_auction_list:
            auction_data_list.append(InteractiveAuctionOutputData(auction))
        return auction_data_list

    def list_request_lots(self, auction_id: int) -> list:
        lot_data_list = []
        self.check_auction_exist(auction_id)
        auction = self.storage.get_interactive_auction(auction_id)
        request_lot_list = self.storage.get_all_request_lots_to_auction(auction_id)
        free_lots_number = auction.cap - len(auction.schedule)
        for lot in request_lot_list:
            lot_data_list.append(LotOutputData(lot))
        return [free_lots_number, lot_data_list]

    def start_auction(self) -> list:
        auction_data_list = []
        interactive_auction_list = self.storage.get_all_interactive_auctions()
        today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        tommorow = today + timedelta(days=1)
        for auction in interactive_auction_list:
            if (today <= auction.start_date) and (auction.start_date < tommorow):
                auction_data_list.append(InteractiveAuctionOutputDataForServices(auction))
        return auction_data_list

    def check_auction_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is None:
            raise RuntimeError("Not find auction with this id")

    def check_auction_not_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is not None:
            raise RuntimeError("Auction with this id exist")

    def check_request_lot_exist(self, auction_id: int, lot_id: int):
        if self.storage.get_request_lot(auction_id, lot_id) is None:
            raise RuntimeError("Not find request lot with this id")

    def check_request_lot_not_exist(self, auction_id: int, lot_id: int):
        if self.storage.get_request_lot(auction_id, lot_id) is not None:
            raise RuntimeError("Request lot with this id exist")

    def check_request_lot_status(self, lot_status: str):
        if lot_status != "opened":
            raise RuntimeError("Wrong lot status")

    def check_auction_lot_not_exist(self, auction_id: int, lot_id: int):
        auction = self.storage.get_interactive_auction(auction_id)
        for auction_lot in auction.schedule:
            if auction_lot.id == lot_id:
                raise RuntimeError("Lot with this id exist")

    def check_lot(self, lot_id: int):
        background_auction = self.storage.get_background_auction()
        for background_auction_lot in background_auction.schedule:
            if background_auction_lot.id == lot_id:
                raise RuntimeError("Lot with this id exist")
