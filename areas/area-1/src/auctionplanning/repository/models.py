from django.db import models
from django.core.validators import MinValueValidator
from datetime import datetime


# Create your models here.
class Auction(models.Model):
    is_approved = models.BooleanField(default=False)
    cap = models.IntegerField(default=1, validators=[MinValueValidator(1, message="cap must be > 0")])
    theme = models.CharField(default="", max_length=100)
    is_opened = models.BooleanField(default=False)
    is_incr = models.BooleanField(default=False)
    is_interactive = models.BooleanField(default=False)
    status = models.CharField(default="created", max_length=10)
    start_date = models.DateTimeField(validators=[MinValueValidator(datetime.now(),
                                      message="start_datetime must be after today")])
    durability = models.IntegerField(default=0)
    employee_id = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.is_approved} {self.cap} {self.theme} {self.is_opened} {self.is_incr}' \
               f'{self.is_interactive} {self.status} {self.start_date} {self.durability} ' \
               f'{self.employee_id}'


class Lot(models.Model):
    auction = models.ForeignKey(Auction, on_delete=models.SET_NULL, null=True)
    start_price = models.FloatField(validators=[MinValueValidator(1, message="start_price must be > 0")])
    description = models.TextField(null=True)
    seller_id = models.IntegerField(default=1)
    quantum = models.IntegerField(default=1, validators=[MinValueValidator(1, message="quantum must be > 0")])
    price_change = models.FloatField(validators=[MinValueValidator(1, message="price_change must be > 0")])
    limit_price = models.FloatField(validators=[MinValueValidator(1, message="limit_price must be > 0")])
    status = models.CharField(default="opened", max_length=10)

    def __str__(self):
        return f'{self.start_price} {self.description} {self.seller_id} {self.quantum} {self.price_change}' \
               f'{self.limit_price} {self.status}'
