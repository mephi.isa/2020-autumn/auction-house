# Generated by Django 3.1.4 on 2020-12-27 17:01

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repository', '0004_auto_20201217_1049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='start_date',
            field=models.DateTimeField(validators=[django.core.validators.MinValueValidator(datetime.datetime(2020, 12, 27, 17, 1, 41, 235815), message='start_datetime must be after today')]),
        ),
    ]
