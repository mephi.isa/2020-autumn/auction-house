# Generated by Django 3.1.4 on 2020-12-16 18:26

import datetime
import django.core.validators
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('repository', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='start_date',
            field=models.DateTimeField(validators=[django.core.validators.MinValueValidator(datetime.datetime(2020, 12, 16, 18, 26, 47, 896305, tzinfo=utc), message='start_datetime must be after today')]),
        ),
    ]
