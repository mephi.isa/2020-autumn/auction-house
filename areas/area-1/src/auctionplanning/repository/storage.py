from typing import List, Optional
from classes.auction_planning import *


class Storage:
    def __init__(self):
        pass

    def get_interactive_auction(self, auction_id) -> Optional[Auction]:
        pass

    def get_all_interactive_auctions(self) -> List[Auction]:
        pass

    def update_interactive_auction(self, auction: Auction) -> int:
        pass

    def add_interactive_auction(self, auction: Auction) -> int:
        pass

    def get_background_auction(self) -> Optional[Auction]:
        pass

    def update_background_auction(self, auction: Auction) -> int:
        pass

    def add_request_lot(self, auction_id: int, lot: Lot) -> int:
        pass

    def get_request_lot(self, auction_id: int, lot_id: int) -> Optional[Lot]:
        pass

    def update_request_lot(self, auction_id: int, lot: Lot) -> int:
        pass

    def delete_request_lot(self, auction_id: int, lot_id: int) -> int:
        pass

    def get_all_request_lots_to_auction(self, auction_id: int) -> List[Lot]:
        pass

