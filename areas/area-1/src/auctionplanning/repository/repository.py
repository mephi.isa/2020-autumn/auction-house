from django.core.exceptions import ObjectDoesNotExist
from repository.storage import *
from repository import models
from datetime import datetime, timedelta


class Repository(Storage):
    @staticmethod
    def _lot_list_by_query_set(query_set) -> List[Lot]:
        lot_list = []
        for lot_object in query_set:
            try:
                lot = Lot(lot_object.id, lot_object.start_price, lot_object.description, lot_object.seller_id,
                          lot_object.quantum, lot_object.price_change, lot_object.limit_price)
                lot.status = lot_object.status
            except ValueError:
                continue
            lot_list.append(lot)
        return lot_list

    @staticmethod
    def _auction_list_by_query_set(query_set) -> List[Auction]:
        auction_list = []
        for auc in query_set:
            try:
                auction = Auction(auc.id, auc.cap, auc.theme, auc.is_opened, auc.is_incr, auc.is_interactive,
                                  auc.start_date, auc.durability, auc.employee_id)
                auction.is_approved = auc.is_approved
                auction.status = auc.status
            except ValueError:
                continue
            try:
                auction.schedule = Repository._lot_list_by_query_set(models.Lot.objects.filter(auction_id=auc.id,
                                                                                               status='approved'))
            except ObjectDoesNotExist:
                auction.schedule = []
            auction_list.append(auction)
        return auction_list

    def get_interactive_auction(self, auction_id) -> Optional[Auction]:
        try:
            auction_object = models.Auction.objects.get(id=auction_id)
        except models.Auction.DoesNotExist:
            return None
        try:
            auction = Auction(auction_object.id, auction_object.cap, auction_object.theme,
                              auction_object.is_opened, auction_object.is_incr,
                              auction_object.is_interactive, auction_object.start_date,
                              auction_object.durability, auction_object.employee_id)
            auction.is_approved = auction_object.is_approved
            auction.status = auction_object.status
        except ValueError:
            return None
        try:
            auction.schedule = Repository._lot_list_by_query_set(models.Lot.objects.filter(auction_id=auction.id,
                                                                                           status='approved'))
        except ObjectDoesNotExist:
            auction.schedule = []
        return auction

    def get_all_interactive_auctions(self) -> List[Auction]:
        return Repository._auction_list_by_query_set(models.Auction.objects.filter(id__gt=1).all())

    def update_interactive_auction(self, auction: Auction) -> int:
        try:
            auction_object = models.Auction.objects.filter(id=auction.id)
        except models.Auction.DoesNotExist:
            return -1
        auction_object.update(is_approved=True, cap=auction.cap, theme=auction.theme, is_opened=auction.is_opened,
                              is_incr=auction.is_incr, is_interactive=auction.is_interactive,
                              status=auction.status, start_date=auction.start_date,
                              durability=auction.durability, employee_id=auction.employee_id)
        for lot in auction.schedule:
            if lot.id == 0:
                models.Lot.objects.create(
                    auction_id=auction.id, start_price=lot.start_price,
                    description=lot.description, seller_id=lot.seller_id, quantum=lot.quantum,
                    price_change=lot.price_change, limit_price=lot.limit_price,
                    status=lot.status
                )
            else:
                models.Lot.objects.filter(id=lot.id).update(
                    auction_id=auction.id, start_price=lot.start_price,
                    description=lot.description, seller_id=lot.seller_id, quantum=lot.quantum,
                    price_change=lot.price_change, limit_price=lot.limit_price,
                    status=lot.status
                )
        return auction.id

    def add_interactive_auction(self, auction: Auction) -> int:
        added_auction = models.Auction.objects.create(
            is_approved=True, cap=auction.cap, theme=auction.theme,
            is_opened=auction.is_opened, is_incr=auction.is_incr,
            is_interactive=auction.is_interactive,
            status=auction.status,
            start_date=auction.start_date,
            durability=auction.durability,
            employee_id=auction.employee_id
        )
        return added_auction.id

    def get_background_auction(self) -> Optional[Auction]:
        try:
            auction_object = models.Auction.objects.get(id=1)
        except models.Auction.DoesNotExist:
            return None
        try:
            auction = Auction(auction_object.id, auction_object.cap, auction_object.theme,
                              auction_object.is_opened, auction_object.is_incr,
                              auction_object.is_interactive, datetime.now() + timedelta(days=1),
                              auction_object.durability, auction_object.employee_id)
            auction.is_approved = auction_object.is_approved
            auction.status = auction_object.status
        except ValueError:
            return None
        try:
            auction.schedule = Repository._lot_list_by_query_set(models.Lot.objects.filter(auction_id=1,
                                                                                           status='approved'))
        except ObjectDoesNotExist:
            auction.schedule = []
        return auction

    def update_background_auction(self, auction: Auction) -> int:
        try:
            auction_object = models.Auction.objects.filter(id=1)
        except models.Auction.DoesNotExist:
            return -1
        auction_object.update(
            is_approved=True, cap=auction.cap, theme=auction.theme,
            is_opened=auction.is_opened, is_incr=auction.is_incr,
            is_interactive=auction.is_interactive,
            status=auction.status,
            start_date=auction.start_date,
            durability=auction.durability,
            employee_id=auction.employee_id
        )
        for lot in auction.schedule:
            if lot.id == 0:
                models.Lot.objects.create(
                    auction_id=auction.id, start_price=lot.start_price,
                    description=lot.description, seller_id=lot.seller_id, quantum=lot.quantum,
                    price_change=lot.price_change, limit_price=lot.limit_price,
                    status=lot.status
                )
            else:
                models.Lot.objects.filter(id=lot.id).update(
                    auction_id=auction.id, start_price=lot.start_price,
                    description=lot.description, seller_id=lot.seller_id, quantum=lot.quantum,
                    price_change=lot.price_change, limit_price=lot.limit_price,
                    status=lot.status
                )
        return auction.id

    def add_request_lot(self, auction_id: int, lot: Lot) -> int:
        added_lot = models.Lot.objects.create(
            auction_id=auction_id, start_price=lot.start_price,
            description=lot.description, seller_id=lot.seller_id, quantum=lot.quantum,
            price_change=lot.price_change, limit_price=lot.limit_price,
            status=lot.status
        )
        return added_lot.id

    def get_request_lot(self, auction_id: int, lot_id: int) -> Optional[Lot]:
        try:
            lot_object = models.Lot.objects.get(id=lot_id, auction_id=auction_id)
        except ObjectDoesNotExist:
            return None
        try:
            lot = Lot(lot_object.id, lot_object.start_price, lot_object.description, lot_object.seller_id,
                      lot_object.quantum, lot_object.price_change, lot_object.limit_price)
            lot.status = lot_object.status
        except ValueError:
            return None
        return lot

    def update_request_lot(self, auction_id: int, lot: Lot) -> int:
        try:
            lot_object = models.Lot.objects.filter(id=lot.id, auction_id=auction_id)
        except ObjectDoesNotExist:
            return -1
        lot_object.update(auction_id=auction_id, start_price=lot.start_price,
                          description=lot.description, seller_id=lot.seller_id,
                          price_change=lot.price_change, limit_price=lot.limit_price,
                          status=lot.status)
        return lot.id

    def delete_request_lot(self, auction_id: int, lot_id: int) -> int:
        try:
            lot_object = models.Lot.objects.filter(id=lot_id, auction_id=auction_id)
        except models.Lot.DoesNotExist:
            return -1
        lot_object.update(auction_id=-1, start_price=1.00,
                          description="", seller_id=-1,
                          price_change=1.00, limit_price=1.00,
                          status="")
        return lot_id

    def get_all_request_lots_to_auction(self, auction_id: int) -> List[Lot]:
        return Repository._lot_list_by_query_set(models.Lot.objects.filter(auction_id=auction_id,
                                                                           status="opened"))
