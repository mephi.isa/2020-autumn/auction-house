"""auctionplanning URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from repository.repository import Repository
from mongo_repository.mongo_repository import MongoRepository
from logic.controller import AuctionPlanningController
from service.views import AuctionPlanningService

repository = MongoRepository()  # repository = Repository()
auction_planning_service = AuctionPlanningService(repository, AuctionPlanningController(repository))

urlpatterns = [
    path('',
         auction_planning_service.main),
    path('add_goods_to_background_auction',
         auction_planning_service.add_goods_to_background_auction),
    path('create_request_list_auctions',
         auction_planning_service.create_request_list_auctions),
    path('change_background_auction',
         auction_planning_service.change_background_auction),
    path('create_interactive_auction',
         auction_planning_service.create_interactive_auction),
    path('action_with_request_list_auctions',
         auction_planning_service.action_with_request_list_auctions),
    path('create_request_to_interactive_auction',
         auction_planning_service.create_request_to_interactive_auction),
    path('action_with_request_to_interactive_auction',
         auction_planning_service.action_with_request_to_interactive_auction),
    path('today_auction_list',
         auction_planning_service.start_auction),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
