from django.contrib import admin
from repository.models import Auction, Lot

# Register your models here.
admin.site.register(Auction)
admin.site.register(Lot)
