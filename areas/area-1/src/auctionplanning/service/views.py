import json
import jsonpickle
from datetime import datetime
from django.db import transaction
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from repository.storage import *
from logic.controller import *
from data.InteractiveAuctionData import *
from data.BackgroundAuctionData import *
from data.LotData import *


def body_to_dict(body: HttpRequest.body) -> dict or list:
    return json.loads(body, object_hook=value_hook)


def value_hook(json_dict):
    for (key, value) in json_dict.items():
        if isinstance(value, bool):
            continue
        try:
            json_dict[key] = int(value)
        except:
            try:
                json_dict[key] = float(value)
            except:
                try:
                    json_dict[key] = datetime.strptime(value, "%Y-%m-%dT%H:%M")
                except:
                    pass
    return json_dict


def return_data(func):
    def wrapper(*args, **kwargs) -> HttpResponse:
        data = func(*args, **kwargs)
        data_json = jsonpickle.encode(data, unpicklable=False)
        return HttpResponse(data_json, content_type='application/json')

    return wrapper


def return_data_template(func):
    def wrapper(*args, **kwargs) -> HttpResponse:
        request, data = func(*args, **kwargs)
        if not data['ok']:
            return render(request, 'error.html', context=data)
        return render(request, f'{data["template"]}.html', context=data)

    return wrapper


# Create your views here.
class AuctionPlanningService:
    def __init__(self, storage: Storage, controller: AuctionPlanningController):
        self.storage = storage
        self.controller = controller

    @staticmethod
    @return_data_template
    def main(request: HttpRequest):
        if request.method == 'PUT':
            try:
                body = body_to_dict(request.body)
            except json.decoder.JSONDecodeError:
                return request, {'ok': False, 'error': 'InvalidBody'}
            if body['role'] == 'planning_role':
                return request, {'ok': True, 'template': 'planning_role'}
            elif body['role'] == 'seller_role':
                return request, {'ok': True, 'template': 'seller_role'}
        elif request.method == 'GET':
            return request, {'ok': True, 'template': 'main'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}

    @transaction.atomic
    @return_data_template
    def add_goods_to_background_auction(self, request: HttpRequest):
        if request.method == 'POST':
            try:
                body = body_to_dict(request.body)
            except json.decoder.JSONDecodeError:
                return request, {'ok': False, 'error': 'InvalidBody'}
            try:
                self.controller.add_goods_to_background_auction(LotInputData(**body))
                return request, {'ok': True, 'action': 'lot added',
                                 'template': 'add_goods_to_background_auction'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
        elif request.method == 'GET':
            return request, {'ok': True, 'template': 'add_goods_to_background_auction'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}
        
    @transaction.atomic
    @return_data_template
    def change_background_auction(self, request: HttpRequest):
        if request.method == 'PUT':
            try:
                body = body_to_dict(request.body)
            except json.decoder.JSONDecodeError:
                return request, {'ok': False, 'error': 'InvalidBody'}
            try:
                auction_id = self.controller.change_background_auction(BackgroundAuctionInputData(**body))
                return request, {'ok': True, 'id': auction_id, 'action': 'changed',
                                 'template': 'change_background_auction'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
        elif request.method == 'GET':
            return request, {'ok': True, 'template': 'change_background_auction'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}

    @transaction.atomic
    @return_data_template
    def create_interactive_auction(self, request: HttpRequest):
        if request.method == 'POST':
            try:
                body = body_to_dict(request.body)
            except json.decoder.JSONDecodeError:
                return request, {'ok': False, 'error': 'InvalidBody'}
            try:
                auction_id = self.controller.create_interactive_auction(InteractiveAuctionInputData(**body))
                return request, {'ok': True, 'id': auction_id, 'action': 'created',
                                 'template': 'create_interactive_auction'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
        elif request.method == 'GET':
            return request, {'ok': True, 'template': 'create_interactive_auction'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}

    @transaction.atomic
    @return_data_template
    def create_request_to_interactive_auction(self, request: HttpRequest):
        try:
            body = body_to_dict(request.body)
        except json.decoder.JSONDecodeError:
            return request, {'ok': False, 'error': 'InvalidBody'}
        if request.method == 'POST':
            try:
                auction_id = body.pop('auction_id')
                lot_id = self.controller.create_request_to_interactive_auction(auction_id,
                                                                               LotInputData(**body))
                return request, {'ok': True, 'auction_id': auction_id, 'id': lot_id,
                                 'action': 'created', 'template': 'create_request_to_interactive_auction'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
        elif request.method == 'PUT':
            return request, {'ok': True, 'auction_id': body['auction_id'],
                             'template': 'create_request_to_interactive_auction'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}

    @transaction.atomic
    @return_data_template
    def action_with_request_to_interactive_auction(self, request: HttpRequest):
        try:
            body = body_to_dict(request.body)
        except json.decoder.JSONDecodeError:
            return request, {'ok': False, 'error': 'InvalidBody'}
        if request.method == 'POST':
            try:
                action = body.pop('action')
                if action == 'approve':
                    lot_id = self.controller.approve_request_to_interactive_auction(**body)
                    lot_data = self.controller.list_request_lots(body['auction_id'])
                    return request, {'ok': True, 'auction_id': body['auction_id'],
                                     'free_lots_number': lot_data[0], 'data': lot_data[1],
                                     'id': lot_id, 'action': 'approved',
                                     'template': 'action_with_request_to_interactive_auction'}
                elif action == 'decline':
                    lot_id = self.controller.decline_request_to_interactive_auction(**body)
                    lot_data = self.controller.list_request_lots(body['auction_id'])
                    return request, {'ok': True, 'auction_id': body['auction_id'],
                                     'free_lots_number': lot_data[0], 'data': lot_data[1],
                                     'id': lot_id, 'action': 'declined',
                                     'template': 'action_with_request_to_interactive_auction'}
                else:
                    return request, {'ok': False, 'error': 'InvalidBody'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
        elif request.method == 'PUT':
            lot_data = self.controller.list_request_lots(body['auction_id'])
            return request, {'ok': True, 'auction_id': body['auction_id'],
                             'free_lots_number': lot_data[0], 'data': lot_data[1],
                             'template': 'action_with_request_to_interactive_auction'}
        else:
            return request, {'ok': False, 'error': 'InvalidMethod'}

    @transaction.atomic
    @return_data_template
    def action_with_request_list_auctions(self, request: HttpRequest):
        if request.method == 'GET':
            try:
                auctions_data = self.controller.list_auctions()
                return request, {'ok': True, 'data': auctions_data, 'template': 'action_with_request_list_auctions'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}

    @transaction.atomic
    @return_data_template
    def create_request_list_auctions(self, request: HttpRequest):
        if request.method == 'GET':
            try:
                auctions_data = self.controller.list_auctions()
                return request, {'ok': True, 'data': auctions_data, 'template': 'create_request_list_auctions'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}

    @transaction.atomic
    @return_data_template
    def list_request_lots(self, request: HttpRequest):
        if request.method == 'GET':
            try:
                body = body_to_dict(request.body)
            except json.decoder.JSONDecodeError:
                return request, {'ok': False, 'error': 'InvalidBody'}
            try:
                lots_data = self.controller.list_request_lots(**body)
                return request, {'ok': True, 'auction_id': body['auction_id'], 'data': lots_data,
                                 'template': 'action_with_request_to_interactive_auction'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}

    @transaction.atomic
    @return_data_template
    def start_auction(self, request: HttpRequest):
        if request.method == 'GET':
            try:
                lots_data = self.controller.start_auction()
                return request, {'ok': True, 'data': lots_data,
                                 'template': 'today_auction_list'}
            except RuntimeError as e:
                return request, {'error': 'RuntimeError', 'ok': False, 'message': str(e)}
