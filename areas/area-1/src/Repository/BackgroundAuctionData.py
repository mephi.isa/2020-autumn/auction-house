from ..Classes.auction_planning import *


class BackgroundAuctionInputData:
    def __init__(self, durability):
        self.durability = durability


class BackgroundAuctionOutputData:
    def __init__(self, auction: Auction):
        self.durability = auction.durability
