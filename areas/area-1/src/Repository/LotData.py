from ..Classes.auction_planning import *


class LotInputData:
    def __init__(self, id, start_price, description, seller_id, quantum,
                 price_change, limit_price):
        self.id = id
        self.start_price = start_price
        self.description = description
        self.seller_id = seller_id
        self.quantum = quantum
        self.price_change = price_change
        self.limit_price = limit_price

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__


class LotOutputData:
    def __init__(self, lot: Lot):
        self.id = lot.id
        self.start_price = lot.start_price
        self.description = lot.description
        self.seller_id = lot.seller_id
        self.quantum = lot.quantum
        self.price_change = lot.price_change
        self.limit_price = lot.limit_price
