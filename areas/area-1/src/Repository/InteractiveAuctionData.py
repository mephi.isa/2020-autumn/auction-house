from ..Classes.auction_planning import *


class InteractiveAuctionInputData:
    def __init__(self, id, cap, theme, is_opened, is_incr, start_datetime, durability,
                 employee_id):
        self.id = id
        self.cap = cap
        self.theme = theme
        self.is_opened = is_opened
        self.is_incr = is_incr
        self.start_datetime = start_datetime
        self.durability = durability
        self.employee_id = employee_id


# for my service
class InteractiveAuctionOutputData:
    def __init__(self, auction: Auction):
        self.id = auction.id
        self.cap = auction.cap
        self.theme = auction.theme
        self.is_opened = auction.is_opened
        self.is_incr = auction.is_incr
        self.start_datetime = auction.start_date
        self.durability = auction.durability
        self.employee_id = auction.employee_id


# for other services
class InteractiveAuctionOutputDataForServices:
    def __init__(self, auction: Auction):
        self.id = auction.id
        self.is_opened = auction.is_opened
        self.is_incr = auction.is_incr
        self.durability = auction.durability
        self.lots = auction.schedule
