import datetime
from datetime import datetime

IsApprovedException = ValueError("auction must be approved for setting auction")
IsNotApprovedException = ValueError("auction must be not approved for setting schedule")

class Auction:
    def __init__(self, id, cap, theme, is_opened, is_incr, is_interactive, start_datetime, durability, \
                        employee_id):
        if not isinstance(id, int):
            raise TypeError("id must be int")
        self.id = id

        self.is_approved = False
        self.set_cap(cap)
        self.set_theme(theme)
        self.set_is_opened(is_opened)
        self.set_is_incr(is_incr)
        self.set_is_interactive(is_interactive)
        self.status = "created"
        self.set_start_datetime(start_datetime)
        self.durability = durability
        self.schedule = []

        if not isinstance(employee_id, int):
            raise TypeError("employee_id must be int")
        if employee_id <= 0:
            raise ValueError("employee_id must be > 0")
        self.employee_id = employee_id

    def set_cap(self, cap):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(cap, int):
            raise TypeError("cap must be int")
        if cap <= 0:
            raise ValueError("cap must be > 0")
        self.cap = cap

    def set_theme(self, theme):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(theme, str):
            raise TypeError("theme must be str")
        self.theme = theme
    
    def set_is_opened(self, is_opened):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(is_opened, bool):
            raise TypeError("is_opened must be bool")
        self.is_opened = is_opened

    def set_is_incr(self, is_incr):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(is_incr, bool):
            raise TypeError("is_incr must be bool")
        self.is_incr = is_incr
    
    def set_is_interactive(self, is_interactive):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(is_interactive, bool):
            raise TypeError("is_interactive must be bool")
        self.is_interactive = is_interactive

    def set_start_datetime(self, start_datetime):
        if self.is_approved:
            raise IsApprovedException
        if not isinstance(start_datetime, datetime):
            raise TypeError("start_datetime must be datetime.datetime")
        if start_datetime <= datetime.now():
            raise ValueError("start_datetime must be after today")
        self.start_date = start_datetime

    def approve(self):
        self.is_approved = True

    def add_lot(self, lot):
        if not self.is_approved:
            raise IsNotApprovedException
        if len(self.schedule) == self.cap:
            raise ValueError("the number of auctions cant exceed the allowed")
        if not isinstance(lot, Lot):
            raise TypeError("lot must be class: Lot")
        self.schedule.append(lot)

    def delete_lot(self, id):
        if not self.is_approved:
            raise IsNotApprovedException
        for num, l in enumerate(self.schedule):
            if l.id == id:
                del self.schedule[num]


class Lot:
    def __init__(self, id, start_price, description, seller_id, quantum, \
                    price_change, limit_price):
        if not isinstance(id, int):
            raise TypeError("id must be int")
        self.id = id

        self.set_start_price(start_price)

        self.description = description

        if not isinstance(seller_id, int):
            raise TypeError("seller_id must be int")
        if seller_id <= 0:
            raise ValueError("seller_id must be > 0")
        self.seller_id = seller_id

        self.set_price_change(price_change)
        self.set_quantum(quantum)
        self.set_limit_price(limit_price)
        self.status = "opened"

    def set_start_price(self, start_price):
        if not isinstance(start_price, float):
            raise TypeError("start_price must be float")
        if start_price <= 0:
            raise ValueError("start_price must be > 0")
        self.start_price = start_price

    def set_quantum(self, quantum):
        if not isinstance(quantum, int):
            raise TypeError("quantum must be int")
        if quantum <= 0:
            raise ValueError("quantum must be > 0")
        self.quantum = quantum

    def set_price_change(self, price_change):
        if not isinstance(price_change, float):
            raise TypeError("price_change must be int")
        if price_change <= 0:
            raise ValueError("price_change must be > 0 and less than start_price")   
        self.price_change = price_change 

    def set_limit_price(self, limit_price):
        if not isinstance(limit_price, float):
            raise TypeError("limit_price must be int")
        if limit_price <= 0:
            raise ValueError("limit_price must be > 0")
        self.limit_price = limit_price 

    def approve(self):
        self.status = "approved"

    def decline(self):
        self.status = "declined"

    def get_status(self):
        return self.status

