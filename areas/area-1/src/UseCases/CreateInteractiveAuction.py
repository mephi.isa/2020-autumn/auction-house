from ..Classes.auction_planning import *
from ..Repository.InteractiveAuctionData import *
from ..Repository.Storage import *


class CreateInteractiveAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, interactive_auction_data: InteractiveAuctionInputData) -> int:
        self.check_auction_not_exist(interactive_auction_data.id)
        auction = Auction(interactive_auction_data.id,
                          interactive_auction_data.cap,
                          interactive_auction_data.theme,
                          interactive_auction_data.is_opened,
                          interactive_auction_data.is_incr,
                          True,
                          interactive_auction_data.start_datetime,
                          interactive_auction_data.durability,
                          interactive_auction_data.employee_id)
        auction_id = self.storage.add_interactive_auction(auction)
        return auction_id

    def check_auction_not_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is not None:
            raise RuntimeError("Auction with this id exist")