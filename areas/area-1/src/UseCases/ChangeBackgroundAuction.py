from ..Classes.auction_planning import *
from ..Repository.BackgroundAuctionData import *


class ChangeBackgroundAuction:
    def __init__(self, storage):
        self.storage = storage

    def run(self, background_auction_data: BackgroundAuctionInputData):
        background_auction = self.storage.get_background_auction()
        background_auction.durability = background_auction_data.durability
        auction_id = self.storage.update_background_auction(background_auction)
        return auction_id
