from ..Classes.auction_planning import *
from ..Repository.Storage import *


class ApproveRequestToInteractiveAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, auction_id: int, lot_id: int) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_exist(auction_id, lot_id)
        auction = self.storage.get_interactive_auction(auction_id)
        request_lot = self.storage.get_request_lot(auction_id, lot_id)
        self.check_request_lot_status(request_lot.get_status())
        self.check_auction_lot_not_exist(auction_id, lot_id)
        request_lot.approve()
        auction.add_lot(request_lot)
        del_lot_id = self.storage.delete_request_lot(auction_id, lot_id)
        self.storage.update_interactive_auction(auction)
        if len(auction.schedule) == auction.cap:
            all_request_lots = self.storage.get_all_request_lots_to_auction(auction_id)
            if all_request_lots is not None:
                for request_lot in all_request_lots:
                    request_lot.decline()
                    self.storage.update_request_lot(auction_id, request_lot)
        return del_lot_id

    def check_auction_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is None:
            raise RuntimeError("Not find auction with this id")

    def check_request_lot_exist(self, auction_id: int, lot_id: int):
        if self.storage.get_request_lot(auction_id, lot_id) is None:
            raise RuntimeError("Not find request lot with this id")

    def check_request_lot_status(self, lot_status: str):
        if lot_status != "opened":
            raise RuntimeError("Wrong lot status")

    def check_auction_lot_not_exist(self, auction_id: int, lot_id: int):
        auction = self.storage.get_interactive_auction(auction_id)
        for auction_lot in auction.schedule:
            if auction_lot.id == lot_id:
                raise RuntimeError("Lot with this id exist")


