from ..Classes.auction_planning import *
from ..Repository.LotData import *
from ..Repository.Storage import *


class CreateRequestToInteractiveAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, auction_id: int, lot_data: LotInputData) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_not_exist(auction_id, lot_data.id)
        lot = Lot(lot_data.id, lot_data.start_price, lot_data.description, lot_data.seller_id,
                  lot_data.quantum, lot_data.price_change, lot_data.limit_price)
        lot_id = self.storage.add_request_lot(auction_id, lot)
        return lot_id

    def check_auction_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is None:
            raise RuntimeError("Not find auction with this id")

    def check_request_lot_not_exist(self, auction_id: int, lot_id: int):
        if self.storage.get_request_lot(auction_id, lot_id) is not None:
            raise RuntimeError("Request lot with this id exist")
