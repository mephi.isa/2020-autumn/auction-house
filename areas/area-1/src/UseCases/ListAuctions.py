from ..Classes.auction_planning import *
from ..Repository.InteractiveAuctionData import *
from ..Repository.Storage import *


class ListAuctions:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self) -> list:
        auction_data_list = []
        interactive_auction_list = self.storage.get_all_interactive_auctions()
        for auction in interactive_auction_list:
            auction_data_list.append(InteractiveAuctionOutputData(auction))
        return auction_data_list
