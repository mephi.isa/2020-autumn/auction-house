from ..Classes.auction_planning import *
from ..Repository.Storage import *


class DeclineRequestToInteractiveAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, auction_id: int, lot_id: int) -> int:
        self.check_auction_exist(auction_id)
        self.check_request_lot_exist(auction_id, lot_id)
        request_lot = self.storage.get_request_lot(auction_id, lot_id)
        self.check_request_lot_status(request_lot.get_status())
        request_lot.decline()
        return request_lot.id

    def check_auction_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is None:
            raise RuntimeError("Not find auction with this id")

    def check_request_lot_exist(self, auction_id: int, lot_id: int):
        if self.storage.get_request_lot(auction_id, lot_id) is None:
            raise RuntimeError("Not find request lot with this id")

    def check_request_lot_status(self, lot_status: str):
        if lot_status != "opened":
            raise RuntimeError("Wrong lot status")
