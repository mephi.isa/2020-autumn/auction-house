from ..Classes.auction_planning import *
from ..Repository.LotData import *
from ..Repository.Storage import *


class AddGoodsToBackgroundAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, lot_data: LotInputData) -> int:
        lot = Lot(lot_data.id, lot_data.start_price, lot_data.description, lot_data.seller_id,
                  lot_data.quantum, lot_data.price_change, lot_data.limit_price)
        self.check_lot(lot.id)
        background_auction = self.storage.get_background_auction()
        lot.approve()
        background_auction.add_lot(lot)
        self.storage.update_background_auction(background_auction)
        return lot_data.id

    def check_lot(self, lot_id: int):
        background_auction = self.storage.get_background_auction()
        for background_auction_lot in background_auction.schedule:
            if background_auction_lot.id == lot_id:
                raise RuntimeError("Lot with this id exist")
