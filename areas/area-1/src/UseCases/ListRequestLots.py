from ..Classes.auction_planning import *
from ..Repository.LotData import *
from ..Repository.Storage import *


class ListRequestLots:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self, auction_id: int) -> list:
        lot_data_list = []
        self.check_auction_exist(auction_id)
        auction = self.storage.get_interactive_auction(auction_id)
        request_lot_list = self.storage.get_all_request_lots_to_auction(auction_id)
        free_lots_number = auction.cap - len(auction.schedule)
        for lot in request_lot_list:
            lot_data_list.append(LotOutputData(lot))
        return [free_lots_number, lot_data_list]

    def check_auction_exist(self, auction_id: int):
        if self.storage.get_interactive_auction(auction_id) is None:
            raise RuntimeError("Not find auction with this id")