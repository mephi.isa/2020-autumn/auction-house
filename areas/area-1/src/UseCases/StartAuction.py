from ..Classes.auction_planning import *
from ..Repository.Storage import *
from ..Repository.InteractiveAuctionData import *
from datetime import datetime, timedelta

class StartAuction:
    def __init__(self, storage: Storage):
        self.storage = storage

    def run(self) -> list:
        auction_data_list = []
        interactive_auction_list = self.storage.get_all_interactive_auctions()
        today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        tommorow = today + timedelta(days=1)
        for auction in interactive_auction_list:
            if (today <= auction.start_date) and (auction.start_date < tommorow):
                auction_data_list.append(InteractiveAuctionOutputDataForServices(auction))
        return auction_data_list
