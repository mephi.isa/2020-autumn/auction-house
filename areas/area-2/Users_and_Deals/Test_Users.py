import unittest
from Users import *


class Test_User(unittest.TestCase):

    def test_user_could_not_be_created_with_invalid_type(self):
        self.assertRaises(TypeError, User, 1.1, 1, 1, 1)

    def test_the_same_deal_could_not_be_added_to_seller_and_buyer_deals_dicts_at_the_same_time(self):
        user = User(1, "", "", "")
        deal1 = Deal(1, 1, 1)
        deal2 = Deal(2, 2, 2)
        user.register_buyers_deal(deal1)
        user.register_seller_deal(deal2)
        self.assertRaises(KeyError, user.register_seller_deal, deal1)
        self.assertRaises(KeyError, user.register_buyers_deal, deal2)

    def test_confirmation_of_card_is_true(self):
        user = User(1, "", "", "")
        user.confirm_card()
        self.assertTrue(user.is_card_confirmed)

    def test_confirmation_of_email_is_true(self):
        user = User(1, "", "", "")
        user.confirm_mail()
        self.assertTrue(user.is_email_confirmed)

    def test_user_created_with_right_attributes(self):
        user = User(1, "", "", "")
        assert user.get_user_id() == 1
        assert user.get_name() == ""
        assert user.get_email() == ""
        assert user.get_phone() == ""

    def test_set_card(self):
        user = User(1, "", "", "")
        card = Card(1, "123", "123", date(3000, 10, 7))
        user.set_card(card)
        assert user.get_card() == card

    def test_is_card_confirmed_is_false_when_setting_new_card(self):
        user = User(1, "", "", "")
        card = Card(1, "123", "123", date(3000, 10, 7))
        user.confirm_card()
        user.set_card(card)
        self.assertFalse(user.is_card_confirmed)


class Test_Card(unittest.TestCase):

    def test_card_could_not_be_created_with_invalid_types(self):
        self.assertRaises(TypeError, Card, 1.1, 1, "123", 1)

    def test_card_could_not_be_created_with_zero_id_and_number(self):
        self.assertRaises(ValueError, Card, 0, 0, 0, date(3000, 10, 7))

    def test_card_last_three_numbers_lenght_is_three(self):
        self.assertRaises(ValueError, Card, 1, "123", "1234", date(3000, 10, 7))

    def test_card_date_is_not_expired(self):
        self.assertRaises(ValueError, Card, 1, "123", "123", date(1997, 10, 7))


class Test_Deal(unittest.TestCase):

    def test_deal_could_not_be_created_with_invalid_type(self):
        self.assertRaises(TypeError, Deal, "", "", "")

    def test_deal_created_with_right_attributes(self):
        deal = Deal(1, 1, 1)
        assert deal.get_deal_id() == 1
        assert deal.get_auct_id() == 1
        assert deal.get_lot_id() == 1

    def test_confirmation_of_send_is_true(self):
        deal = Deal(1, 1, 1)
        deal.confirm_send()
        self.assertTrue(deal.is_sent)

    def test_confirmation_of_delivery_is_true(self):
        deal = Deal(1, 1, 1)
        deal.confirm_deliverence()
        self.assertTrue(deal.is_delivered)

    def test_commission_calculation(self):
        deal = Deal(1, 1, 1)
        commission = deal.commission_calculation(100)
        self.assertNotEqual(0, commission)

    def test_succesful_deal_is_true(self):
        deal = Deal(1, 1, 1)
        deal.confirm_send()
        deal.confirm_deliverence()
        self.assertTrue(deal.sucsessful_deal())

    def test_succesful_deal_is_false(self):
        deal = Deal(1, 1, 1)
        self.assertFalse(deal.sucsessful_deal())

