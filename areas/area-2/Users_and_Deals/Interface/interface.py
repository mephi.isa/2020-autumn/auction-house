from Users import User, Card

class DataBase:
    def __init__(self):
        pass

    def get_user_by_id(self, id) -> User:
        pass

    def is_new(self, new_user) -> bool:
        pass

    def save(self, user) -> User:
        pass

    def save_mail_confirmation(self, user):
        pass

    def save_user_card(self, user):
        pass

    def get_user_by_email(self, email) -> User:
        pass

    def get_card(self, id) -> Card:
        pass

    def get_user_by_deal_id(self, deal_id) -> User:
        pass

    def get_seller_by_deal_lot(self, lot_id) -> User:
        pass

    def save_card(self, card):
        pass

    def save_deal(self, deal, user):
        pass


class AuctionService:

    def is_lot_valid(self, auction_id, lot_id) -> bool:
        pass

    def get_lot_cost(self, lot_id) -> int:
        pass


class BankService:
    def __init__(self):
        pass

    def pay(self, card, amount) -> bool:
        pass

    def pay_back(self, card, amount) -> bool:
        pass

    def freeze(card, amount):
        pass

    def unfreeze(card, amount):
        pass


class MailService:

    def __init__(self):
        pass

    def send(self, user):
        pass

    def get_key(self, user) -> str:
        pass


