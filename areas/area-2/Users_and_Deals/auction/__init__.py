import os

import click
from flask import Flask, g
from flask.cli import with_appcontext

from auction.db import SQLiteDB


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='5327276e6ca815ae038cf07a38f68975',
        DATABASE=os.path.join(app.instance_path, 'auction.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import account
    app.register_blueprint(account.bp)

    @app.route('/')
    def error():
        return 'Error'
    return app


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    if 'db' not in g:
        g.db = SQLiteDB.init_db()
    click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(SQLiteDB.close_db)
    app.cli.add_command(init_db_command)

