from datetime import date

from auction.Users import Card, User


class Database:
    def __init__(self):
        self.users = []
        self.confirmed = {}
        kwargs1 = {"card": Card(0, "111111111111111", "111", date.fromisocalendar(2021, 1, 1))}
        user1 = User(0, "user1", "user1@gmail.com", "+111111111111", **kwargs1)
        self.users.append(user1)

    def get_user_by_id(self, _id):
        self.current_int = _id
        for user in self.users:
            if user.id == _id:
                return user

    def get_user_by_name(self, username):
        self.current_name = username
        for user in self.users:
            if user.get_name() == username:
                return user


    def get_seller_deals(self, id):
        self.current_seller_int = id

    def get_buyer_deals(self, id):
        self.current_buyer_int = id


class DatabaseMailConfirmed:
    def __init__(self):
        self.users = []
        self.confirmed = {}
        kwargs1 = {"card": Card(0, "111111111111111", "111", date.fromisocalendar(2021, 1, 1))}
        user1 = User(0, "user1", "user1@gmail.com", "+111111111111", **kwargs1)
        user1.confirm_mail()
        self.users.append(user1)

    def get_user_by_name(self, username):
        self.current_name = username
        for user in self.users:
            if user.get_name() == username:
                return user
