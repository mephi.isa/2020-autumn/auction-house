import sqlite3
from datetime import datetime

from flask import current_app, g
from auction.Users import User, Card, Deal


class SQLiteDB:
    @staticmethod
    def get_db():
        if 'db' not in g:
            g.db = sqlite3.connect(
                current_app.config['DATABASE'],
                detect_types=sqlite3.PARSE_DECLTYPES
            )
            g.db.row_factory = sqlite3.Row
        return g.db

    @staticmethod
    def close_db(e=None):
        db = g.pop('db', None)

        if db is not None:
            db.close()

    @staticmethod
    def init_db():
        db = SQLiteDB.get_db()

        with current_app.open_resource('schema.sql') as f:
            db.executescript(f.read().decode('utf8'))
        return db

    def get_user_by_id(self, id) -> User:
        db = self.get_db()
        val = db.execute(
            'SELECT * FROM user WHERE id = ''?'';', (id,)
        ).fetchone()
        if not val:
            raise ValueError('Пользователь не найден')
        user = User(id=val['id'], name=val['name'], email=val['email'], phone=val['phone'])
        if val['is_mail_confirmed'] == 0:
            user.is_email_confirmed = False
        else:
            user.is_email_confirmed = True
        if val['is_card_confirmed'] == 0:
            user.is_card_confirmed = False
        else:
            user.is_card_confirmed = True
        user.set_card(self.get_card(user.get_user_id()))
        user.seller_deals = self.get_seller_deals(user.get_user_id())
        user.buyer_deals = self.get_buyer_deals(user.get_user_id())
        return user

    def is_new(self, new_user) -> bool:
        db = self.get_db()
        val = db.execute(
            'SELECT id FROM user WHERE name = ''?'' or email = ''?'' or phone = ''?'';',
            (new_user.get_name(), new_user.get_email(), new_user.get_phone())
        ).fetchone()
        print(val)
        if val is not None:
            return False
        else:
            return True

    def save(self, user) -> User:
        db = self.get_db()
        try:
            db.execute(
                'INSERT INTO user (name, email, phone) VALUES (?, ?, ?);',
                (user.get_name(), user.get_email(), user.get_phone())
            )
            db.commit()
        except sqlite3.IntegrityError:
            raise ValueError('Данные пользователя не уникальны!')
        return self.get_user_by_name(user.get_name())

    def save_mail_confirmation(self, user):
        if self.is_new(new_user=user) is False:
            db = self.get_db()
            db.execute(
                'UPDATE user SET is_mail_confirmed = 1 WHERE id = ?',
                (user.get_user_id(),)
            )
            db.commit()
        else:
            raise ValueError('Пользователь не найден')

    def save_user_card(self, user):
        db = self.get_db()
        card = user.get_card()
        if self.get_card(card.id) is None:
            db.execute(
                'INSERT INTO card(id, number, three_dig, expire_date) VALUES (?, ''?'', ''?'', ?);',
                (card.id, card.number, card.three_num, datetime.combine(card.due_date, datetime.min.time()).timestamp())
            )
            db.commit()
        else:
            if user.is_card_confirmed is True:
                db.execute(
                    'UPDATE user SET is_card_confirmed = 1 WHERE id = ?', (user.get_user_id(),)
                )
                db.commit()

    def get_user_by_email(self, email) -> User:
        db = self.get_db()
        val = db.execute(
            'SELECT * FROM user WHERE email = ''?'';', (email,)
        ).fetchone()
        if not val:
            return None
        else:
            user = User(id=val['id'], name=val['name'], email=val['email'], phone=val['phone'])
            if val['is_mail_confirmed'] == 0:
                user.is_email_confirmed = False
            else:
                user.is_email_confirmed = True

            if val['is_card_confirmed'] == 0:
                user.is_card_confirmed = False
            else:
                user.is_card_confirmed = True
            return user

    def get_card(self, id) -> Card:
        db = self.get_db()
        val = db.execute(
            'SELECT * FROM card WHERE id = ?', (id, )
        ).fetchone()
        if val is None:
            return None
        else:
            card = Card(int(val['id']),
                        val['number'],
                        val['three_dig'],
                        datetime.fromtimestamp(int(val['expire_date'])).date())
            return card

    def get_user_by_deal_id(self, deal_id) -> User:
        pass

    def get_seller_by_deal_lot(self, lot_id) -> User:
        db = self.get_db()
        var = db.execute(
            'SELECT seller_id FROM deal WHERE lot_id = ?;', (lot_id,)
        ).fetchone()
        return self.get_user_by_id(var['seller_id'])

    def save_deal(self, deal, user):
        db = self.get_db()
        val = db.execute(
            'SELECT * FROM deal WHERE lot_id = ?', (deal.get_lot_id(), )
        ).fetchone()
        if self.is_selling(deal=deal, user=user):
            if val and deal.is_sent is True and deal.is_delivered is False:
                db.execute(
                    'UPDATE deal SET is_sent = ? WHERE lot_id = ?', (1, deal.get_lot_id())
                )
            elif val:
                raise ValueError('Лот для продажи уже присутствует в системе')
            else: db.execute(
                    'INSERT INTO deal(lot_id, auction_id, seller_id) VALUES (?, ?, ?)',
                    (deal.lot_id, deal.auct_id, user.get_user_id())
                )
            db.commit()
        else:
            if deal.is_sent is False and deal.is_delivered is False:
                if val['seller_id'] == user.get_user_id():
                    raise ValueError('Продавец не может выступать покупателем')
                db.execute(
                    'UPDATE deal SET buyer_id = ? WHERE lot_id = ?;', (user.get_user_id(), deal.get_lot_id())
                )
            elif deal.is_sent is True and deal.is_delivered is False:
                db.execute(
                    'UPDATE deal SET is_sent = ? WHERE lot_id = ?', (1, deal.get_lot_id())
                )
            else:
                db.execute(
                    'UPDATE deal SET is_delivered = 1, is_sent = 1 WHERE lot_id = ?', (deal.get_lot_id(), )
                )
            db.commit()

    def is_selling(self, deal, user):
        if user.get_buyer_deals().get(deal.lot_id):
            return False
        elif user.get_seller_deals().get(deal.lot_id):
            return True
        else:
            raise ValueError('Сделка без продавца и покупателя')

    def get_user_by_name(self, name):
        db = self.get_db()
        val = db.execute(
            'SELECT * FROM user WHERE name = ''?'';', (name,)
        ).fetchone()
        if not val:
            raise ValueError('Пользователь не найден')

        user = User(id=val['id'], name=val['name'], email=val['email'], phone=val['phone'])
        if val['is_mail_confirmed'] == 0:
            user.is_email_confirmed = False
        else:
            user.is_email_confirmed = True

        if val['is_card_confirmed'] == 0:
            user.is_card_confirmed = False
        else:
            user.is_card_confirmed = True
        return user

    def get_seller_deals(self, seller_id):
        db = self.get_db()
        vars = db.execute(
            'SELECT * FROM deal WHERE seller_id = ?;', (seller_id,)
        ).fetchall()
        if vars is None:
            return None
        deals = {}
        for var in vars:
            deals[var['lot_id']] = self.form_deal(var)
        return deals

    def get_buyer_deals(self, buyer_id):
        db = self.get_db()
        vars = db.execute(
            'SELECT * FROM deal WHERE buyer_id = ?;', (buyer_id,)
        ).fetchall()
        if vars is None:
            return None
        deals = {}
        for var in vars:
            deals[var['lot_id']] = self.form_deal(var)
        return deals

    def form_deal(self, var):
        deal = Deal(id=var['id'], lot_id=var['lot_id'], auct_id=var['auction_id'])
        if var['is_sent'] == 1:
            deal.is_sent = True
        else:
            deal.is_sent = False
        if var['is_delivered'] == 1:
            deal.is_delivered = True
        else:
            deal.is_delivered = False

        return deal
