import hashlib
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class MailService:

    server = None

    def __init__(self):

        self.text = "http://127.0.0.1:5000/auth/{email}/{hash}"
        self.password = "12345tyuiop@"
        self.email = "auctionuser9@gmail.com"


    def send(self, user):
        msg = MIMEMultipart()
        message = hashlib.md5(user.get_name().encode('utf-8'))

        msg['From'] = self.email
        msg['To'] = user.get_email()
        msg['Subject'] = "Подтверждение почты"

        url = self.text.format(email=msg['To'], hash=message.hexdigest())
        msg.attach(MIMEText(url, "plain"))
        MailService.server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        # MailService.server.starttls()
        MailService.server.login(self.email, self.password)
        MailService.server.sendmail(msg['From'], [msg['To']], msg.as_string())
        MailService.server.quit()

    def get_key(self, user) -> str:
        return hashlib.md5(user.get_name().encode('utf-8')).hexdigest()
