import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from auction.mail import MailService
from auction.service import RegistrationService, DealService, AccountService
from auction import SQLiteDB as DataBase
from auction.dependency import BankService, AuctionService

bp = Blueprint('account', __name__, url_prefix='/account')

db = DataBase()
bank = BankService()
mail = MailService()
auction = AuctionService()
registration_service = RegistrationService(data_base=db, bank_service=bank, mail_service=mail)
deal_service = DealService(data_base=db, bank_service=bank, auction_service=auction)
account = AccountService(data_base=db)


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)

    return wrapped_view


@bp.route('/')
@login_required
def index():
    seller_deals = account.load_deals(session.get('user_id'), True)
    buyer_deals = account.load_deals(session.get('user_id'), False)
    return render_template('account/index.html', seller_deals=seller_deals, buyer_deals=buyer_deals)


@bp.route('/deal_registration', methods=('GET', 'POST'))
@login_required
def dealregist():
    if request.method == 'POST':
        type = request.form['deal_type']
        user_id = session.get('user_id')
        lot_id = int(request.form['lot_id'])
        auct_id = int(request.form['auct_id'])

        error = None
        try:
            if type == 'seller':
                deal_service.register_seller_deal(user_id=user_id, lot_id=lot_id, auct_id=auct_id)
            else:
                deal_service.register_buyer_deal(user_id=user_id, lot_id=lot_id, auct_id=auct_id)
            return redirect(url_for('account.index'))
        except ValueError as e:
            error = e.args[0]
        flash(error)
    return render_template('account/newdeal.html')


@bp.route('/send/<int:lot_id>')
@login_required
def send(lot_id):
    user_id = session.get('user_id')
    deal_service.confirm_item_send(user_id=user_id, lot_id=lot_id)
    return redirect(url_for('account.index'))

@bp.route('/receive/<int:lot_id>')
@login_required
def receive(lot_id):
    user_id = session.get('user_id')
    deal_service.confirm_item_received(user_id=user_id, lot_id=lot_id)
    return redirect(url_for('account.index'))
