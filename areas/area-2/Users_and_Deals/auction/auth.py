import functools
from datetime import datetime, date

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from auction.mail import MailService
from auction.service import RegistrationService, AccountService
from auction import SQLiteDB as DataBase
from auction.dependency import BankService

bp = Blueprint('auth', __name__, url_prefix='/auth')

db = DataBase()
bank = BankService()
mail = MailService()
service = RegistrationService(data_base=db, bank_service=bank, mail_service=mail)
account = AccountService(data_base=db)


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view

@bp.route('/register', methods=('GET', 'POST'))
def register():
    session.clear()
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        error = None
        try:
           service.registration(username=name, phone=phone, email=email)
        except ValueError as e:
            error = str(e)
        if error is None:
            flash('Подтвердите адрес электронной почты для активации профиля')
            return redirect(url_for('auth.login'))
        else:
            flash(error)
    return render_template('auth/register.html')


@bp.route('/<mail>/<key>')
def confirm_mail(mail, key):
    error = None
    try:
        service.confirm_mail(email=mail, key=key)
    except ValueError as e:
        error = e.args[0]

    if error is None:
        flash('Адрес электронной почты подтвержден')
        return redirect(url_for('auth.login'))
    else:
        flash(error)
        return redirect(url_for('auth.login'))


@bp.route('/login', methods=('GET', 'POST'))
def login():
    session.clear()
    if request.method == 'POST':
        name = request.form['name']
        error = None
        try:
            user = account.login(username=name)
        except ValueError as e:
            error = e.args[0]
        if error is None:
            session.clear()
            session['user_id'] = user.get_user_id()
            return redirect(url_for('account.index'))
        flash(error)
    return render_template('auth/login.html')


@bp.route('/card', methods=('GET', 'POST'))
@login_required
def card():
    if request.method == 'POST':
        card_number = request.form['card_number']
        due_date = datetime.strptime(
            request.form['due_date'],
            '%Y-%m').date()
        three_dig = request.form['three_dig']
        user_id = session.get('user_id')
        error = None

        if len(card_number) != 12:
            error = 'Номер карты должен состоять из 12 цифр'
        if len(three_dig) != 3:
            error = 'Секретный код должен состоять из 3 цифр'
        if due_date < date.today():
            error = 'Устаревшая дата'

        if error is None:
            try:
                result = service.confirm_bank_card(user_id=user_id,
                                          number=card_number,
                                          three_num=three_dig,
                                          due_date=due_date)
                if result:
                    return redirect(url_for('account.index'))
            except ValueError as e:
                error = e
            flash(error)
    card = db.get_card(session.get('user_id'))
    return render_template('auth/card.html', card=card)


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.login'))


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    try:
        if user_id is None:
            g.user = None
        else:
            g.user = account.load_user(user_id=user_id)
    except:
        session.clear()
        return redirect(url_for('auth.login'))

