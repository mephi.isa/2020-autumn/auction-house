import os
import tempfile
import unittest
from datetime import datetime, date

from auction import create_app
from auction.Users import User, Deal, Card
from auction.db import SQLiteDB as DataBase


class TestSqlite(unittest.TestCase):

    def setUp(self) -> None:
        db_fd, db_path = tempfile.mkstemp()

        app = create_app({
            'TESTING': True,
            'DATABASE': db_path,
        })

        # app = create_app()

        with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
            self._data_sql = f.read().decode('utf8')

            # DataBase.get_db()

        self.app = app
        self.client = self.app.test_client()
        self.runner = self.app.test_cli_runner()
        os.unlink(db_path)

    def test_is_new_existing_user(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            # name exists
            user = User(-1, 'user0', 'user@mail.com', '+000000000000')
            assert DataBase().is_new(user) == False
            # email exists
            user = User(-1, 'user4', 'user0@mail.com', '+000000000000')
            assert DataBase().is_new(user) == False
            # phone exists
            user = User(-1, 'user4', 'user4@mail.com', '+998900000000')
            assert DataBase().is_new(user) == False

    def test_is_new_new_user(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = User(-1, 'user4', 'user4@mail.com', '+000000000000')
            assert DataBase().is_new(user) == True

    def test_get_user_by_name(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = DataBase().get_user_by_name('user0')
            assert user.get_user_id() == 1
            assert user.get_name() == 'user0'
            assert user.get_email() == 'user0@mail.com'
            assert user.get_phone() == '+998900000000'

    def test_get_user_by_name_not_found(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            with self.assertRaises(ValueError) as cm:
                DataBase().get_user_by_name('user100')
            self.assertEqual(
                'Пользователь не найден',
                str(cm.exception)
            )

    def test_save_user(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = User(-1, 'user4', 'user4@mail.com', '+998940000000')
            new_user = DataBase().save(user)
            assert new_user.get_name() == 'user4'
            assert new_user.get_email() == 'user4@mail.com'
            assert new_user.get_phone() == '+998940000000'

    def test_save_user_with_not_unique_value(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            with self.assertRaises(ValueError) as cm:
                user = User(-1, 'user0', 'user0@mail.com', '+998900000000')
                DataBase().save(user)
            self.assertEqual(
                'Данные пользователя не уникальны!',
                str(cm.exception)
            )

    def test_get_user_by_email(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = DataBase().get_user_by_email('user0@mail.com')
            assert user.get_user_id() == 1
            assert user.get_name() == 'user0'
            assert user.get_email() == 'user0@mail.com'
            assert user.get_phone() == '+998900000000'
            assert user.is_card_confirmed is False
            assert user.is_email_confirmed is False

    def test_get_user_by_email_user_not_found(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = DataBase().get_user_by_email('user5@mail.com')
            assert user is None

    def test_save_mail_confirmation_for_existing_user(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            user = User(1, 'user0', 'user0@mail.com', '+998900000000')
            db.save_mail_confirmation(user)
            user = db.get_user_by_email('user0@mail.com')
            assert user.is_email_confirmed is True

    def test_save_mail_confirmation_if_user_not_found(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            user = User(-1, 'user4', 'user4@mail.com', '+998940000000')
            with self.assertRaises(ValueError) as cm:
                db.save_mail_confirmation(user)
            self.assertEqual(
                'Пользователь не найден',
                str(cm.exception)
            )

    def test_get_user_by_id(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            user = DataBase().get_user_by_id(1)
            assert user.get_user_id() == 1
            assert user.get_name() == 'user0'
            assert user.get_email() == 'user0@mail.com'
            assert user.get_phone() == '+998900000000'
            assert user.is_card_confirmed is False
            assert user.is_email_confirmed is False

    def test_get_user_by_id_if_user_not_found(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            with self.assertRaises(ValueError) as cm:
                DataBase().get_user_by_id(5)
            self.assertEqual(
                'Пользователь не найден',
                str(cm.exception)
            )

    def test_is_selling_seller_deal(self):
        user = User(1, 'user4', 'user4@mail.com', '+998940000000')
        deal = Deal(-1, 1, 1)
        user.register_seller_deal(deal)
        assert DataBase().is_selling(deal=deal, user=user) is True

    def test_is_selling_buyer_deal(self):
        user = User(1, 'user4', 'user4@mail.com', '+998940000000')
        deal = Deal(-1, 1, 1)
        user.register_buyers_deal(deal)
        assert DataBase().is_selling(deal=deal, user=user) is False

    def test_is_selling_deal_not_registered(self):
        user = User(1, 'user4', 'user4@mail.com', '+998940000000')
        deal = Deal(-1, 1, 1)
        with self.assertRaises(ValueError) as cm:
            DataBase().is_selling(deal=deal, user=user)
        self.assertEqual(
            'Сделка без продавца и покупателя',
            str(cm.exception)
        )

    def test_save_seller_deal(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            user = db.get_user_by_id(1)
            deal = Deal(-1, 2, 2)
            user.register_seller_deal(deal)
            db.save_deal(deal, user)

            val = db.get_db().execute(
                'SELECT * FROM deal WHERE lot_id = ?', (deal.get_lot_id(), )
            ).fetchone()

            assert val['lot_id'] == deal.get_lot_id()

    def test_save_seller_deal_deal_exists(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            user = db.get_user_by_id(1)
            deal = Deal(-1, 1, 1)
            with self.assertRaises(ValueError) as cm:
                db.save_deal(deal, user)
            self.assertEqual(
                'Лот для продажи уже присутствует в системе',
                str(cm.exception)
            )

    def test_save_buyer_deal(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            user = db.get_user_by_id(2)
            deal = Deal(-1, 6, 1)
            user.register_buyers_deal(deal)
            db.save_deal(deal, user)
            val = db.get_db().execute(
                'SELECT * FROM deal WHERE lot_id = ?', (deal.get_lot_id(),)
            ).fetchone()

            assert val['lot_id'] == deal.get_lot_id()
            assert val['auction_id'] == deal.get_auct_id()
            assert val['seller_id'] == 1
            assert val['buyer_id'] == 2

    def test_get_card_if_not_found(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            assert db.get_card(5) is None

    def test_get_card(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            card = db.get_card(1)
            assert card.id == 1
            assert card.number == '123412341234'
            assert card.three_num == '123'
            assert card.due_date == datetime(2023, 12, 17).date()

    def test_save_card(self):
        with self.app.app_context():
            DataBase.init_db()
            DataBase.get_db().executescript(self._data_sql)
            db = DataBase()
            now = date.today()
            due_date = date(now.year+3, now.month, now.day)
            user = db.get_user_by_id(2)
            card = Card(2, '123412341235', '125', due_date)
            user.set_card(card)
            db.save_user_card(user)
            card = db.get_card(2)
            assert card.id == 2
            assert card.number == '123412341235'
            assert card.three_num == '125'
            assert card.due_date == due_date

    def test_save_card_confirmed(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            user = db.get_user_by_id(1)
            card = db.get_card(1)
            user.set_card(card)
            user.confirm_card()
            db.save_user_card(user)
            user = db.get_user_by_id(1)
            card = db.get_card(1)
            assert card.id == 1
            assert card.number == '123412341234'
            assert card.three_num == '123'


    def test_get_seller_deals(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            deals = db.get_seller_deals(1)
            assert deals[1].lot_id == 1
            assert deals[1].auct_id == 1
            assert deals[1].is_sent == False
            assert deals[1].is_delivered == False
            assert deals[3].lot_id == 3
            assert deals[3].auct_id == 1
            assert deals[3].is_sent == False
            assert deals[3].is_delivered == False
            assert deals[4].lot_id == 4
            assert deals[4].auct_id == 1
            assert deals[4].is_sent == False
            assert deals[4].is_delivered == False

    def test_get_buyer_deals(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            deals = db.get_buyer_deals(2)
            assert deals[1].lot_id == 1
            assert deals[1].auct_id == 1
            assert deals[1].is_sent == False
            assert deals[1].is_delivered == False
            assert deals[3].lot_id == 3
            assert deals[3].auct_id == 1
            assert deals[3].is_sent == False
            assert deals[3].is_delivered == False
            assert deals[4].lot_id == 4
            assert deals[4].auct_id == 1
            assert deals[4].is_sent == False
            assert deals[4].is_delivered == False

    def test_get_seller_by_deal_lot(self):
        with self.app.app_context():
            db = DataBase()
            db.init_db()
            db.get_db().executescript(self._data_sql)
            user = db.get_seller_by_deal_lot(1)
            assert user.get_user_id() == 1
            assert user.get_name() == 'user0'
            assert user.get_email() == 'user0@mail.com'
            assert user.get_phone() == '+998900000000'


if __name__ == '__main__':
    unittest.main()
