DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS card;
DROP TABLE IF EXISTS deal;

CREATE TABLE user(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    email TEXT UNIQUE NOT NULL,
    phone TEXT UNIQUE NOT NULL,
    is_mail_confirmed INTEGER NOT NULL
                 CHECK (is_mail_confirmed in (0, 1))
                 DEFAULT 0,
    is_card_confirmed INTEGER NOT NULL
                 CHECK (is_card_confirmed in (0, 1))
                 DEFAULT 0
);

CREATE TABLE card(
    id INTEGER PRIMARY KEY NOT NULL ,
    number TEXT UNIQUE NOT NULL,
    three_dig TEXT UNIQUE NOT NULL,
    expire_date INTEGER NOT NULL,
    FOREIGN KEY (id) REFERENCES user (id)
);

CREATE TABLE deal(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    lot_id INTEGER UNIQUE NOT NULL,
    auction_id INTEGER NOT NULL,
    is_sent INTEGER NOT NULL
                 CHECK (is_sent in (0, 1))
                 DEFAULT 0,
    is_delivered INTEGER NOT NULL
                 CHECK (is_delivered in (0, 1))
                 DEFAULT 0,
    seller_id INTEGER NOT NULL,
    buyer_id INTEGER,
    FOREIGN KEY (seller_id) REFERENCES user (id),
    FOREIGN KEY (buyer_id) REFERENCES user (id)
);