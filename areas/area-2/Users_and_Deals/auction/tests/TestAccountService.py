import unittest

from auction.Dependency.AccountDependency import Database, DatabaseMailConfirmed
from auction.service import AccountService


class TestAccountService(unittest.TestCase):
    def test_login_mail_not_confirmed(self):
        db = Database()
        account = AccountService(db)
        with self.assertRaises(ValueError) as cm:
            account.login('user1')
        self.assertEqual(
            'Необходимо активировать аккаунт',
            str(cm.exception)
        )
        assert db.current_name == 'user1'

    def test_login_(self):
        db = DatabaseMailConfirmed()
        account = AccountService(db)
        user = account.login('user1')
        assert db.current_name == 'user1'
        assert user.get_name() == 'user1'
        assert user.get_email() == 'user1@gmail.com'
        assert user.get_phone() == '+111111111111'

    def test_load_user(self):
        db = Database()
        account = AccountService(db)
        user = account.load_user(0)
        assert db.current_int == 0
        assert user.get_name() == 'user1'
        assert user.get_email() == 'user1@gmail.com'
        assert user.get_phone() == '+111111111111'

    def test_load_seller_deals(self):
        db = Database()
        account = AccountService(db)
        account.load_deals(0, is_seller=True)
        assert db.current_seller_int == 0

    def test_load_buyer_deals(self):
        db = Database()
        account = AccountService(db)
        account.load_deals(0, is_seller=False)
        assert db.current_buyer_int == 0

if __name__ == '__main__':
    unittest.main()
