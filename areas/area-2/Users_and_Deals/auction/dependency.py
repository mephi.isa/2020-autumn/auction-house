
class AuctionService:

    def is_lot_valid(self, auction_id, lot_id) -> bool:
        return True

    def get_lot_cost(self, lot_id) -> int:
        pass


class BankService:
    def __init__(self):
        pass

    def pay(self, card, amount) -> bool:
        return True

    def pay_back(self, card, amount) -> bool:
        pass

    def freeze(card, amount):
        pass

    def unfreeze(card, amount):
        pass


class MailService:

    def __init__(self):
        pass

    def send(self, user):
        pass

    def get_key(self, user) -> str:
        return user.get_name()
