import datetime
from enum import Enum

from business_objects.lot import LotInteractive, LotClose


class AuctionStatus(Enum):
    PREPARATION = 1
    STARTED = 2
    ENDED = 3


class Auction:
    def __init__(self, id_auction):
        self.current_lot = -1
        self.lots = []
        self.id_auction = id_auction
        self.status = AuctionStatus.PREPARATION
        self.start_date = datetime.datetime.now()
        self.end_date = datetime.datetime(1, 1, 1)

    def auction_not_going(self):
        if self.status != AuctionStatus.STARTED:
            return True
        return False

    def next_lot(self):
        if self.auction_not_going():
            return
        if len(self.lots) == self.current_lot + 1:
            raise ValueError("no lots")
        self.current_lot += 1
        self.lots[self.current_lot].lot_go()

    def start_auction(self):
        if self.end_date != datetime.datetime(1, 1, 1):
            raise ValueError("auction finished")
        if len(self.lots) == 0:
            raise ValueError("lots equal zero")
        self.status = AuctionStatus.STARTED
        self.start_date = datetime.datetime.now()
        self.current_lot += 1
        self.lots[self.current_lot].lot_go()

    def end_auction(self):
        if self.status != AuctionStatus.ENDED:
            self.status = AuctionStatus.ENDED
            self.end_date = datetime.datetime.now()

    def get_id_auction(self):
        return self.id_auction

    def get_data(self):
        return self.start_date

    def get_end_data(self):
        return self.end_date

    def end_current_lot(self):
        win, price = self.lots[self.current_lot].define_winner()
        if len(self.lots) != self.current_lot + 1:
            self.next_lot()
        else:
            self.end_auction()
        return win, price

    def current_lot_id(self):
        return self.lots[self.current_lot].id_lot


class AuctionInteractiveRaiseOpen(Auction):
    def add_lot(self, id_lot):
        self.lots.append(LotInteractive(id_lot))

    def check_time(self, date_now):
        return date_now + datetime.timedelta(seconds=30)

    def create_bid(self, id_participant, price):
        if self.auction_not_going():
            raise ValueError("This auction is not active")
        date_now = datetime.datetime.now()
        if self.lots[self.current_lot].create_bid(id_participant, price):
            self.finish_time = self.lots[self.current_lot].get_finish_time()
            if self.check_time(date_now) >= self.finish_time:
                self.lots[self.current_lot].set_finish_time(self.finish_time + datetime.timedelta(seconds=30))


class AuctionInteractiveFall(Auction):

    def current_lot_id(self):
        return self.lots[self.current_lot][1].id_lot

    def add_lot(self, id_lot, price):
        self.lots.append([price, LotInteractive(id_lot)])

    def start_auction(self):
        if self.end_date != datetime.datetime(1, 1, 1):
            raise ValueError("auction finished")
        if len(self.lots) == 0:
            raise ValueError("lots equal zero")
        self.status = AuctionStatus.STARTED
        self.start_date = datetime.datetime.now()
        self.current_lot += 1
        self.lots[self.current_lot][1].lot_go()

    def adopted_bid(self, id_participant):
        if self.auction_not_going():
            raise ValueError("This auction is not active")
        date_now = datetime.datetime.now()
        lot = self.lots[self.current_lot]
        lot[1].create_bid(id_participant, lot[0])
        lot[1].set_finish_time(date_now)

    def extend_lot(self, minutes_to_finish):
        if self.auction_not_going():
            return
        lot = self.lots[self.current_lot]
        new_finish_time = lot[1].get_finish_time() + datetime.timedelta(minutes=minutes_to_finish)
        lot[1].set_finish_time(new_finish_time)
        lot[0] = lot[0] - (lot[0] / 100) * 5

    def next_lot(self):
        if self.auction_not_going():
            return
        if len(self.lots) == self.current_lot:
            raise ValueError("no lots")
        self.current_lot += 1
        self.lots[self.current_lot][1].lot_go()

    def end_current_lot(self):
        win, price = self.lots[self.current_lot][1].define_winner()
        if len(self.lots) != self.current_lot + 1:
            self.next_lot()
        else:
            self.end_auction()
        return win, price

    def current_price(self):
        return self.lots[self.current_lot][0]


class AuctionInteractiveClose(Auction):

    def add_lot(self, id_lot):
        self.lots.append(LotClose(id_lot))

    def create_bid(self, id_participant, price):
        if self.auction_not_going():
            raise ValueError("This auction is not active")
        self.lots[self.current_lot].create_bid(id_participant, price)
