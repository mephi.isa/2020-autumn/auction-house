import datetime


class Bid:

    def __init__(self, id_participant, price):
        if price <= 0:
            raise ValueError("price cannot be zero or negative")
        self.id_participant = id_participant
        self.price = price
        self.timeRate = datetime.datetime.now()

    def getIdParticipant(self):
        return self.id_participant

    def getPrice(self):
        return self.price

    def getTimeRate(self):
        return self.timeRate
