import datetime

from business_objects.bid import Bid


class Lot:

    def __init__(self, id_lot):
        self.start_time = datetime.datetime(1, 1, 1)
        self.finish_time = datetime.datetime(1, 1, 1)
        self.id_lot = id_lot
        self.bids = []

    def create_bid(self, id_participant, price):
        now_time = datetime.datetime.now()
        if self.start_time == datetime.datetime(1, 1, 1):
            raise ValueError("lot is not active, early")
        if self.finish_time <= now_time:
            raise ValueError("lot is not active, later")
        if len(self.bids) == 0:
            self.bids.append(Bid(id_participant, price))
            return 1
        if id_participant == self.bids[-1].getIdParticipant():
            raise ValueError("you have already created bid")
        if price <= self.bids[-1].getPrice():
            raise ValueError("price cannot be less or equal than last Bid")
        self.bids.append(Bid(id_participant, price))
        return 1

    def get_id_lot(self):
        return self.id_lot

    def get_finish_time(self):
        return self.finish_time

    def get_start_time(self):
        return self.start_time

    def define_winner(self):
        if datetime.datetime.now() < self.finish_time:
            raise ValueError("lot did not finish")
        if len(self.bids) == 0:
            return None, None
        else:
            return self.bids[-1].getIdParticipant(), self.bids[-1].getPrice()

    def current_price(self):
        return self.bids[-1].getIdParticipant(), self.bids[-1].getPrice()


class LotInteractive(Lot):

    def __init__(self, id_lot, lot_duration=5):
        super().__init__(id_lot)
        self.bids = []
        self.lot_duration = lot_duration

    def __set_lot_duration__(self, lot_duration):
        self.lot_duration = lot_duration

    def set_finish_time(self, finish_time):
        if self.finish_time == datetime.datetime(1, 1, 1):
            self.finish_time = finish_time
            return
        if datetime.datetime.now() >= self.finish_time:
            raise ValueError("lot finished")
        self.finish_time = finish_time

    def lot_go(self):
        self.start_time = datetime.datetime.now()
        self.set_finish_time(self.start_time + datetime.timedelta(minutes=self.lot_duration))


class LotNonInteractive(Lot):
    def __init__(self, id_lot, start_time, minutes_to_finish):
        super().__init__(id_lot)
        self.start_time = start_time
        self.finish_time = start_time + datetime.timedelta(minutes=minutes_to_finish)
        self.bids = []


class LotClose(LotInteractive):

    def __init__(self, id_lot, lot_duration=5):
        self.start_time = datetime.datetime(1, 1, 1)
        self.finish_time = datetime.datetime(1, 1, 1)
        self.bids = {}

        self.id_lot = id_lot
        self.lot_duration = lot_duration

    def create_bid(self, id_participant, price):
        now_time = datetime.datetime.now()
        if self.start_time == datetime.datetime(1, 1, 1):
            raise ValueError("lot is not active, early")
        if self.finish_time < now_time:
            raise ValueError("lot is not active, later")
        if id_participant in self.bids:
            raise ValueError("you have already created bid in this close auction")
        self.bids[id_participant] = Bid(id_participant, price)
        return 1

    def define_winner(self):
        if datetime.datetime.now() < self.finish_time:
            raise ValueError("lot did not finish")
        max_price = 0
        time_winner = datetime.datetime.now()
        id_participant = 0
        for bid in self.bids:
            if max_price == self.bids[bid].getPrice():
                if self.bids[bid].getTimeRate() < time_winner:
                    id_participant = bid
                    time_winner = self.bids[bid].getTimeRate()
            if max_price < self.bids[bid].getPrice():
                id_participant = bid
                max_price = self.bids[bid].getPrice()
                time_winner = self.bids[bid].getTimeRate()
        return id_participant, max_price
