from business_component.data.interactive_auction_data import InteractiveAuctionData
from business_component.data.lot_result import LotResult
from business_component.interfaces.bank_system import IntegrationWithBank
from business_component.interfaces.db import *
from business_component.interfaces.notifications import Notifier
from business_component.interfaces.timer import Timer
from business_objects.auction import *


class HoldingAnAuction:
    def __init__(self, db, participants_db, timer, notifier, bank_operations):
        self.db = db
        self.participants_db = participants_db
        self.timer = timer
        self.notifier = notifier
        self.bank_operations = bank_operations

    def new_auction(self, auction_data):
        auction = self.create_auction(auction_data)
        self.db.save_new_auction(auction)
        self.db.save_lots(auction_data.lots)
        self.timer.schedule_auction_start(auction)

    def joining_an_auction(self, auction_id, user_id):
        self.participants_db.add(auction_id, user_id)

    def start_auction(self, auction_id):
        auction = self.db.load_auction(auction_id)
        participants = self.participants_db.get_auction_participants(auction_id)
        self.notifier.send_notification(participants, auction)
        auction.start_auction()

        current_lot = self.db.load_lot(auction.current_lot_id())
        self.notifier.notify_new_lot(participants, current_lot)
        self.timer.schedule_lot_finish(auction)

    def make_bid(self, auction_id, user_id, price):
        auction = self.db.load_auction(auction_id)
        if type(auction) is AuctionInteractiveFall:
            raise ValueError("You can't place a bid for a lower bid auction")
        auction.create_bid(user_id, price)
        self.db.update_bid(auction_id, user_id, price)

        self.timer.reschedule_lot_finish(auction)

        participants = self.participants_db.get_auction_participants(auction_id)
        self.notifier.notify_new_bid(participants, auction_id, price, user_id)

    def agreement_with_bid(self, auction_id, user_id):
        auction = self.db.load_auction(auction_id)
        if not type(auction) is AuctionInteractiveFall:
            raise ValueError("Agreement is only for auction with lower bids")
        auction.adopted_bid(user_id)
        price = auction.current_price()
        self.db.update_bid(auction_id, user_id, price)

        participants = self.participants_db.get_auction_participants(auction_id)
        self.notifier.notify_new_bid(participants, auction_id, price, user_id)
        self.finish_lot(auction_id)

    def extend_lot_for_auction_with_lower_bids(self, auction_id):
        auction = self.db.load_auction(auction_id)
        if not type(auction) is AuctionInteractiveFall:
            raise ValueError("You can extend lot only for a lower bid auction")

        current_lot = self.db.load_lot(auction.current_lot_id())
        current_price = auction.lots[auction.current_lot][0]
        if current_price > current_lot.min_price:
            auction.extend_lot(2)
            participants = self.participants_db.get_auction_participants(auction_id)
            self.notifier.notify_new_bid(participants, auction_id, auction.lots[auction.current_lot][0])
            self.timer.reschedule_lot_finish(auction)
        else:
            self.timer.schedule_lot_finish(auction)

    def finish_lot(self, auction_id):
        auction = self.db.load_auction(auction_id)
        current_lot = self.db.load_lot(auction.current_lot_id())
        winner_id, price = auction.end_current_lot()

        participants = self.participants_db.get_auction_participants(auction_id)
        self.notifier.notify_lot_winner(participants, auction, winner_id)
        if winner_id is not None:
            self.db.save_information_about_the_winner(current_lot.lot_id, winner_id, price)
            self.notifier.send_lot_result(
                LotResult(winner_id, current_lot.seller_id, current_lot.lot_id,
                          auction_id, price))
            self.bank_operations.freezing_funds(winner_id)

        self.db.save_completed_lot(auction, current_lot)

        if auction.status != AuctionStatus.ENDED:
            new_lot_data = self.db.load_lot(auction.current_lot_id())
            self.notifier.notify_new_lot(participants, new_lot_data)
            self.timer.schedule_lot_finish(auction)
        else:
            self.notifier.send_notification(participants, auction)

    def create_auction(self, auction_data: InteractiveAuctionData) -> Auction:
        if auction_data.is_opened:
            if auction_data.is_incr:
                auction = AuctionInteractiveRaiseOpen(auction_data.auction_id)
                for lot in auction_data.lots:
                    auction.add_lot(lot.lot_id)
                return auction
            else:
                auction = AuctionInteractiveFall(auction_data.auction_id)
                for lot in auction_data.lots:
                    auction.add_lot(lot.lot_id, lot.start_price)
                return auction
        else:
            auction = AuctionInteractiveClose(auction_data.auction_id)
            for lot in auction_data.lots:
                auction.add_lot(lot.lot_id)
            return auction
