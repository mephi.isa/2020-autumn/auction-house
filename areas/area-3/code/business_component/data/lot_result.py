class LotResult:
    def __init__(self, buyer_id, seller_id, lot_id, auction_id, price):
        self.buyer_id = buyer_id
        self.seller_id = seller_id
        self.lot_id = lot_id
        self.auction_id = auction_id
        self.price = price
