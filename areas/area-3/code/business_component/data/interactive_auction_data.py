class LotInformation:
    def __init__(self, lot_id, description,  finish_time, seller_id, start_price=None, min_price=None):
        self.lot_id = lot_id
        self.description = description
        self.start_price = start_price
        self.finish_time = finish_time
        self.min_price = min_price
        self.seller_id = seller_id


class InteractiveAuctionData:
    def __init__(self, auction_id, is_opened, is_incr, durability, lots: [LotInformation]):
        self.auction_id = auction_id
        self.is_opened = is_opened
        self.is_incr = is_incr
        self.durability = durability
        self.lots = lots

