class DatabaseFunctions:

    # запись информации о победителе в базу
    def save_information_about_the_winner(self, lot_id, user_id, price):
        pass

    def save_new_auction(self, auction):
        pass

    def load_auction(self, auction_id):
        pass

    def save_lots(self, lots):
        pass

    def load_lot(self, lot_id):
        pass

    def save_started_auction(self, auction):
        pass

    def update_bid(self, auction_id, user_id, price):
        pass

    def save_completed_lot(self, auction, current_lot):
        pass


class ParticipantDatabaseFunctions:
    def add(self, auction_id, participant_id):
        pass

    def get_auction_participants(self, auction_id):
        pass
