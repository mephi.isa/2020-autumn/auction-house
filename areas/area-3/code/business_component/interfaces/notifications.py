from business_component.data.lot_result import LotResult


class Notifier:
    def __init__(self):
        pass

    # рассылка уведомлений пользователям, зарегистрированным на аукцион
    def send_notification(self, participants, auction):
        pass

    def notify_new_lot(self, participants, current_lot):
        pass

    def notify_lot_winner(self, participants, auction, winner_data):
        pass
    
    # отправка информации о ставке
    def notify_new_bid(self, participants, auction_id, price, user_id=None):
        pass

    # отправка результата аукциона для конкретного лота в систему для совершения сделок
    def send_lot_result(self, lot_result: LotResult):
        pass
