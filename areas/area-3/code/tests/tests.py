import time

import pytest

from business_objects.auction import *
from business_objects.lot import *


def test_lot_non_interactive_could_be_created():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)


def test_bid_could_not_be_created():
    with pytest.raises(ValueError) as e:
        b = Bid(1, 0)
    assert str(e.value) == "price cannot be zero or negative"


def test_bid_could_not_be_added_in_lot_with_not_correct_price():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)
    Lot.create_bid(1, 100)
    assert Lot.bids[-1].getIdParticipant() == 1
    assert Lot.bids[-1].getPrice() == 100
    with pytest.raises(ValueError) as e:
        Lot.create_bid(4, 50)

    assert str(e.value) == "price cannot be less or equal than last Bid"


def test_bid_could_not_be_added_in_lot_last_participant():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)
    Lot.create_bid(1, 100)
    with pytest.raises(ValueError) as e:
        Lot.create_bid(1, 1000)

    assert str(e.value) == "you have already created bid"


def test_could_be_create_4_bid():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)
    Lot.create_bid(1, 100)
    assert Lot.bids[-1].getIdParticipant() == 1
    assert Lot.bids[-1].getPrice() == 100
    Lot.create_bid(2, 500)
    assert Lot.bids[-1].getIdParticipant() == 2
    assert Lot.bids[-1].getPrice() == 500
    Lot.create_bid(4, 1000)
    assert Lot.bids[-1].getIdParticipant() == 4
    assert Lot.bids[-1].getPrice() == 1000
    Lot.create_bid(2, 1500)
    assert Lot.bids[-1].getIdParticipant() == 2
    assert Lot.bids[-1].getPrice() == 1500
    assert len(Lot.bids) == 4


def test_could_be_create_1_bid():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)
    Lot.create_bid(1, 100)
    assert Lot.bids[-1].getIdParticipant() == 1
    assert Lot.bids[-1].getPrice() == 100
    with pytest.raises(ValueError):
        Lot.create_bid(4, 100)
        Lot.create_bid(1, 500)
        Lot.create_bid(2, 99)
    assert len(Lot.bids) == 1


def test_could_not_be_created_later_bid():
    startTime = datetime.datetime.now()
    minutesToFinish = 0
    Lot = LotNonInteractive(1113, startTime, minutesToFinish)
    assert Lot.get_id_lot() == 1113
    assert Lot.get_start_time() == startTime
    assert Lot.get_finish_time() == startTime + datetime.timedelta(minutes=minutesToFinish)
    with pytest.raises(ValueError) as e:
        Lot.create_bid(1, 100)
    assert str(e.value) == "lot is not active, later"


def test_lot_interactive_no_start_and_not_recieve_bid():
    startTime = datetime.datetime.now()
    minutesToFinish = 1000
    Lot = LotInteractive(1113)
    with pytest.raises(ValueError) as e:
        Lot.create_bid(1, 100)
    assert str(e.value) == "lot is not active, early"


def test_lot_interactive_start_and_recieve_bid():
    Lot = LotInteractive(1113)
    assert Lot.get_id_lot() == 1113
    Lot.lot_go()
    Lot.create_bid(1, 100)
    assert Lot.bids[-1].getIdParticipant() == 1
    assert Lot.bids[-1].getPrice() == 100
    assert len(Lot.bids) == 1


def test_lot_close_do_no_recieve_participant_2_times():
    Lot = LotClose(1113, 100)
    assert Lot.get_id_lot() == 1113
    Lot.lot_go()
    Lot.create_bid(1, 100)
    assert Lot.bids[1].getIdParticipant() == 1
    assert Lot.bids[1].getPrice() == 100
    time.sleep(0.0001)
    Lot.create_bid(2, 100)
    assert Lot.bids[2].getIdParticipant() == 2
    assert Lot.bids[2].getPrice() == 100
    Lot.create_bid(3, 50)
    assert Lot.bids[3].getIdParticipant() == 3
    assert Lot.bids[3].getPrice() == 50
    with pytest.raises(ValueError) as e:
        Lot.create_bid(1, 1000)
    assert str(e.value) == "you have already created bid in this close auction"
    assert Lot.bids[1].getIdParticipant() == 1
    assert Lot.bids[1].getPrice() == 100
    assert len(Lot.bids) == 3
    t = datetime.datetime.now()
    Lot.set_finish_time(t)
    time.sleep(0.0001)
    win, price = Lot.define_winner()
    assert price == 100
    assert win == 1


def test_auction_could_be_create():
    beforeDate = datetime.datetime.now()
    auction = AuctionInteractiveRaiseOpen(1000)
    assert auction.get_id_auction() == 1000
    afterDate = datetime.datetime.now()
    assert auction.get_id_auction() == 1000
    res = auction.get_data() >= beforeDate
    assert res


def test_auction_could_not_be_create_bid():
    auction = AuctionInteractiveRaiseOpen(1000)
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    with pytest.raises(ValueError) as e:
        auction.create_bid(1, 1000)
    assert str(e.value) == "This auction is not active"


def test_auction_could_not_be_create_bid_with_less_price():
    auction = AuctionInteractiveRaiseOpen(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.lots[-1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.create_bid(1, 1000)
    with pytest.raises(ValueError) as e:
        auction.create_bid(2, 500)
    assert str(e.value) == "price cannot be less or equal than last Bid"


def test_auction_could_not_be_create_bid_with_equals_price():
    auction = AuctionInteractiveRaiseOpen(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.lots[-1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.create_bid(1, 1000)
    with pytest.raises(ValueError) as e:
        auction.create_bid(2, 1000)
    assert str(e.value) == "price cannot be less or equal than last Bid"


def test_auction_could_not_be_start_without_lots():
    auction = AuctionInteractiveRaiseOpen(1000)
    with pytest.raises(ValueError) as e:
        auction.start_auction()
    assert str(e.value) == "lots equal zero"


def test_auction_could_not_be_create_bid_with_last_participant():
    auction = AuctionInteractiveRaiseOpen(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.lots[-1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.create_bid(1, 1000)
    with pytest.raises(ValueError) as e:
        auction.create_bid(1, 1500)
    assert str(e.value) == "you have already created bid"


def test_auction_close_could_not_be_create_bid_participant_2_times():
    auction = AuctionInteractiveClose(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.lots[-1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.create_bid(1, 1000)
    assert auction.lots[0].bids[1].getPrice() == 1000
    auction.create_bid(2, 1000)
    assert auction.lots[0].bids[2].getPrice() == 1000
    with pytest.raises(ValueError) as e:
        auction.create_bid(1, 1050)
    assert str(e.value) == "you have already created bid in this close auction"


def test_auction_fall_could_not_be_create_bid_second_times():
    auction = AuctionInteractiveFall(1000)
    auction.next_lot()
    assert auction.get_id_auction() == 1000
    auction.add_lot(1, 100)
    assert auction.lots[-1][1].get_id_lot() == 1
    auction.add_lot(2, 200)
    assert auction.lots[-1][1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.adopted_bid(1)
    with pytest.raises(ValueError) as e:
        auction.adopted_bid(2)
    assert str(e.value) == "lot is not active, later"
    with pytest.raises(ValueError) as e:
        auction.lots[0][1].create_bid(2, 1000)
    assert str(e.value) == "lot is not active, later"


def test_auction_fall_could_be_create_bid_for_two_lots():
    auction = AuctionInteractiveFall(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1, 100)
    assert auction.lots[-1][1].get_id_lot() == 1
    auction.add_lot(2, 200)
    assert auction.lots[-1][1].get_id_lot() == 2
    assert len(auction.lots) == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.extend_lot(1)
    auction.adopted_bid(1)
    time.sleep(0.00001)
    win, price = auction.end_current_lot()
    assert win == 1
    assert price == 95
    auction.adopted_bid(1)
    time.sleep(0.00001)
    win, price = auction.end_current_lot()
    assert win == 1
    assert price == 200
    auction.end_auction()
    with pytest.raises(ValueError) as e:
        auction.adopted_bid(1)
    assert str(e.value) == "This auction is not active"
    with pytest.raises(ValueError) as e:
        auction.start_auction()
    assert str(e.value) == "auction finished"


def test_auction_change_finish():
    auction = AuctionInteractiveRaiseOpen(1000)
    auction.next_lot()
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    finishTime = auction.lots[0].get_finish_time()
    auction.create_bid(1, 1000)
    assert auction.lots[0].bids[-1].getIdParticipant() == 1
    assert auction.lots[0].bids[-1].getPrice() == 1000
    j = finishTime == auction.lots[0].get_finish_time()
    assert j
    checkTime = lambda dateNow: dateNow + datetime.timedelta(seconds=650)
    auction.check_time = checkTime
    auction.create_bid(2, 2000)
    i = finishTime < auction.lots[0].get_finish_time()
    assert i


def test_lot_close_define_winner():
    Lot = LotClose(1113)
    assert Lot.get_id_lot() == 1113
    Lot.lot_go()
    Lot.create_bid(1, 100)
    assert Lot.bids[1].getIdParticipant() == 1
    assert Lot.bids[1].getPrice() == 100
    time.sleep(0.0001)
    Lot.create_bid(2, 100)
    assert Lot.bids[2].getIdParticipant() == 2
    assert Lot.bids[2].getPrice() == 100
    Lot.create_bid(3, 50)
    assert Lot.bids[3].getIdParticipant() == 3
    assert Lot.bids[3].getPrice() == 50
    Lot.create_bid(4, 1000)
    assert Lot.bids[4].getIdParticipant() == 4
    assert Lot.bids[4].getPrice() == 1000
    Lot.create_bid(5, 500)
    assert Lot.bids[5].getIdParticipant() == 5
    assert Lot.bids[5].getPrice() == 500
    time.sleep(0.0001)
    Lot.create_bid(14, 1000)
    assert Lot.bids[14].getIdParticipant() == 14
    assert Lot.bids[14].getPrice() == 1000
    t = datetime.datetime.now()
    Lot.set_finish_time(t)
    time.sleep(0.0001)
    win, price = Lot.define_winner()
    assert price == 1000
    assert win == 4


def test_auction_raise_define_winner():
    auction = AuctionInteractiveRaiseOpen(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1)
    assert auction.lots[-1].get_id_lot() == 1
    auction.add_lot(2)
    assert auction.lots[-1].get_id_lot() == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    auction.create_bid(1, 1000)
    assert auction.lots[0].bids[-1].getIdParticipant() == 1
    assert auction.lots[0].bids[-1].getPrice() == 1000
    with pytest.raises(ValueError) as e:
        auction.lots[1].create_bid(7, 9)
    assert str(e.value) == "lot is not active, early"
    auction.create_bid(2, 1001)
    assert auction.lots[0].bids[-1].getIdParticipant() == 2
    assert auction.lots[0].bids[-1].getPrice() == 1001
    auction.create_bid(6, 3001)
    assert auction.lots[0].bids[-1].getIdParticipant() == 6
    assert auction.lots[0].bids[-1].getPrice() == 3001
    auction.create_bid(3, 3003)
    assert auction.lots[0].bids[-1].getIdParticipant() == 3
    assert auction.lots[0].bids[-1].getPrice() == 3003
    with pytest.raises(ValueError) as e:
        auction.create_bid(10, 3003)
    assert str(e.value) == "price cannot be less or equal than last Bid"
    auction.lots[0].set_finish_time(datetime.datetime.now())
    time.sleep(0.0001)
    win, price = auction.end_current_lot()
    assert win == 3
    assert price == 3003
    auction.create_bid(2, 1001)
    assert auction.lots[1].bids[-1].getIdParticipant() == 2
    assert auction.lots[1].bids[-1].getPrice() == 1001
    auction.create_bid(3, 2001)
    assert auction.lots[1].bids[-1].getIdParticipant() == 3
    assert auction.lots[1].bids[-1].getPrice() == 2001
    auction.create_bid(4, 5000)
    assert auction.lots[1].bids[-1].getIdParticipant() == 4
    assert auction.lots[1].bids[-1].getPrice() == 5000
    auction.create_bid(2, 6000)
    assert auction.lots[1].bids[-1].getIdParticipant() == 2
    assert auction.lots[1].bids[-1].getPrice() == 6000
    auction.lots[1].set_finish_time(datetime.datetime.now())
    time.sleep(0.0001)
    with pytest.raises(ValueError) as e:
        auction.next_lot()
    assert str(e.value) == "no lots"
    win, price = auction.end_current_lot()
    assert win == 2
    assert price == 6000
    auction.end_auction()
    assert auction.auction_not_going()


def test_auction_fall_extend_finish():
    auction = AuctionInteractiveFall(1000)
    assert auction.get_id_auction() == 1000
    auction.add_lot(1, 100)
    assert auction.lots[-1][1].get_id_lot() == 1
    auction.add_lot(2, 200)
    assert auction.lots[-1][1].get_id_lot() == 2
    assert auction.auction_not_going()
    auction.start_auction()
    assert auction.auction_not_going() == False
    t = auction.lots[0][1].get_finish_time()
    auction.extend_lot(5)
    assert auction.lots[0][1].get_finish_time() == t + datetime.timedelta(minutes=5)
