import pytest

from business_component.data.interactive_auction_data import InteractiveAuctionData, LotInformation
from business_component.interfaces.bank_system import IntegrationWithBank
from business_component.interfaces.notifications import Notifier
from business_component.interfaces.timer import Timer
from business_component.use_cases.holding_an_auction import HoldingAnAuction
from business_objects.auction import *
from mock.db_mock import DatabaseFunctionsMock, ParticipantDatabaseFunctionsMock


class TestHoldingAnAuction:

    def test_holding_an_auction_with_lower_bids_with_agreement_with_one_lot(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 50)
        auction_data = InteractiveAuctionData(auction_id, True, False, 1,
                                              [first_lot])

        mocked_end_new_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_auction_start')
        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 1
        auction = database_mock.auctions.get(auction_id)
        mocked_end_new_auction.assert_called_with(auction)
        assert auction.status == AuctionStatus.PREPARATION

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_notifier_send_notification = mocker.patch.object(holding_an_auction.notifier, 'send_notification')
        mocked_notifier_new_lot = mocker.patch.object(holding_an_auction.notifier, 'notify_new_lot')
        mocked_end_start_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        assert auction.status == AuctionStatus.STARTED
        mocked_notifier_send_notification.assert_called_with(participants_database_mock.participants[auction_id],
                                                             auction)
        mocked_notifier_new_lot.assert_called_with(participants_database_mock.participants[auction_id],
                                                   database_mock.lots[first_lot_id])
        mocked_end_start_auction.assert_called_with(auction)

        mocked_notify_new_bid = mocker.patch.object(holding_an_auction.notifier, 'notify_new_bid')
        mocked_notify_lot_winner = mocker.patch.object(holding_an_auction.notifier, 'notify_lot_winner')
        mocked_send_lot_result = mocker.patch.object(holding_an_auction.notifier, 'send_lot_result')
        mocked_freezing_funds = mocker.patch.object(holding_an_auction.bank_operations, 'freezing_funds')
        holding_an_auction.agreement_with_bid(auction_id, user_id)
        assert auction.lots[auction.current_lot][1].bids[0].getIdParticipant() == user_id
        mocked_notify_new_bid.assert_called_with(participants_database_mock.participants[auction_id],
                                                 auction_id,
                                                 auction.current_price(),
                                                 user_id)
        mocked_notify_lot_winner.assert_called_with(participants_database_mock.participants[auction_id],
                                                    auction,
                                                    user_id)
        mocked_send_lot_result.assert_called()
        mocked_freezing_funds.assert_called_with(user_id)
        mocked_notifier_send_notification.assert_called_with(participants_database_mock.participants[auction_id],
                                                             auction)

        assert database_mock.winners[first_lot_id] == (user_id, first_lot_start_price)
        assert auction.status == AuctionStatus.ENDED

    def test_holding_an_auction_with_lower_bids_with_agreement_with_few_lots(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 50)
        second_lot_id = 15
        second_lot_start_price = 200
        second_lot = LotInformation(second_lot_id, "description", datetime.datetime.now(), 1, second_lot_start_price,
                                    50)
        auction_data = InteractiveAuctionData(auction_id, True, False, 1,
                                              [first_lot, second_lot])

        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 2
        auction = database_mock.auctions.get(auction_id)

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_end_start_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        mocked_end_start_auction.assert_called_with(auction)

        with pytest.raises(ValueError) as e:
            holding_an_auction.make_bid(auction_id, user_id, 100)
        assert str(e.value) == "You can't place a bid for a lower bid auction"

        mocked_notify_new_lot = mocker.patch.object(holding_an_auction.notifier, 'notify_new_lot')
        mocked_end_schedule_lot_finish = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.agreement_with_bid(auction_id, user_id)
        mocked_notify_new_lot.assert_called_with(participants_database_mock.participants[auction_id],
                                                 database_mock.lots[second_lot_id])
        mocked_end_schedule_lot_finish.assert_called_with(auction)

        assert database_mock.winners[first_lot_id] == (user_id, first_lot_start_price)
        assert auction.status == AuctionStatus.STARTED

        holding_an_auction.agreement_with_bid(auction_id, user_id)
        assert auction.status == AuctionStatus.ENDED

    def test_holding_an_auction_with_lower_bids_with_extend_lot(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 50)
        second_lot_id = 15
        second_lot_start_price = 200
        second_lot = LotInformation(second_lot_id, "description", datetime.datetime.now(), 1, second_lot_start_price,
                                    50)

        auction_data = InteractiveAuctionData(auction_id, True, False, 1,
                                              [first_lot, second_lot])
        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 2
        auction = database_mock.auctions.get(auction_id)

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_end_start_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        mocked_end_start_auction.assert_called_with(auction)

        mocked_end_reschedule_lot_finish = mocker.patch.object(holding_an_auction.timer, 'reschedule_lot_finish')
        mocked_notify_new_bid = mocker.patch.object(holding_an_auction.notifier, 'notify_new_bid')
        next_price = first_lot_start_price - (first_lot_start_price / 100) * 5
        holding_an_auction.extend_lot_for_auction_with_lower_bids(auction_id)
        assert auction.lots[auction.current_lot][0] == next_price
        mocked_notify_new_bid.assert_called_with(participants_database_mock.participants[auction_id],
                                                 auction_id,
                                                 next_price)
        mocked_end_reschedule_lot_finish.assert_called_with(auction)

        holding_an_auction.agreement_with_bid(auction_id, user_id)

        assert database_mock.winners[first_lot_id] == (user_id, next_price)

    def test_holding_an_auction_with_lower_bids_with_lot_to_min_price(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 95)

        auction_data = InteractiveAuctionData(auction_id, True, False, 1,
                                              [first_lot])
        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 1
        auction = database_mock.auctions.get(auction_id)

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_end_extend_lot = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        mocked_end_extend_lot.assert_called_with(auction)

        next_price = first_lot_start_price - (first_lot_start_price / 100) * 5
        holding_an_auction.extend_lot_for_auction_with_lower_bids(auction_id)
        assert auction.lots[auction.current_lot][0] == next_price

        mocked_end_extend_lot = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.extend_lot_for_auction_with_lower_bids(auction_id)
        assert auction.lots[auction.current_lot][0] == next_price

        mocked_end_extend_lot.assert_called_with(auction)

        auction.lots[auction.current_lot][1].finish_time = datetime.datetime.now()
        holding_an_auction.finish_lot(auction_id)
        assert len(database_mock.winners) == 0
        assert auction.status == AuctionStatus.ENDED

    def test_holding_an_auction_with_bids_for_increase_with_agreement_with_few_lots(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 50)
        second_lot_id = 15
        second_lot_start_price = 200
        second_lot = LotInformation(second_lot_id, "description", datetime.datetime.now(), 1, second_lot_start_price,
                                    50)
        auction_data = InteractiveAuctionData(auction_id, True, True, 1,
                                              [first_lot, second_lot])

        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 2
        auction = database_mock.auctions.get(auction_id)

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_end_start_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        mocked_end_start_auction.assert_called_with(auction)

        with pytest.raises(ValueError) as e:
            holding_an_auction.agreement_with_bid(auction_id, user_id)
        assert str(e.value) == "Agreement is only for auction with lower bids"

        mocked_end_reschedule_lot_finish = mocker.patch.object(holding_an_auction.timer, 'reschedule_lot_finish')
        mocked_end_make_bid = mocker.patch.object(holding_an_auction.notifier, 'notify_new_bid')
        participants = participants_database_mock.get_auction_participants(auction_id)
        holding_an_auction.make_bid(auction_id, user_id, 100)
        mocked_end_reschedule_lot_finish.assert_called_with(auction)
        mocked_end_make_bid.assert_called_with(participants, auction_id, 100, user_id)
        holding_an_auction.make_bid(auction_id, 2, 200)
        mocked_end_make_bid.assert_called_with(participants, auction_id, 200, 2)

        with pytest.raises(ValueError) as e:
            holding_an_auction.extend_lot_for_auction_with_lower_bids(auction_id)
        assert str(e.value) == "You can extend lot only for a lower bid auction"

        finish_time_before_bid = datetime.datetime.now() + datetime.timedelta(seconds=10)
        auction.lots[auction.current_lot].finish_time = finish_time_before_bid

        last_price = 300
        holding_an_auction.make_bid(auction_id, user_id, last_price)
        mocked_end_make_bid.assert_called_with(participants, auction_id, last_price, user_id)
        finish_time_after_bid = auction.lots[auction.current_lot].finish_time
        assert finish_time_after_bid - finish_time_before_bid == datetime.timedelta(seconds=30)

        auction.lots[auction.current_lot].finish_time = datetime.datetime.now()
        holding_an_auction.finish_lot(auction_id)
        assert database_mock.winners[first_lot_id] == (user_id, last_price)

    def test_holding_an_close_auction(self, mocker):
        database_mock = DatabaseFunctionsMock()
        participants_database_mock = ParticipantDatabaseFunctionsMock()
        holding_an_auction = HoldingAnAuction(database_mock, participants_database_mock, Timer(),
                                              Notifier(),
                                              IntegrationWithBank())

        auction_id = 1
        first_lot_id = 5
        first_lot_start_price = 100
        first_lot = LotInformation(first_lot_id, "description", datetime.datetime.now(), 1, first_lot_start_price, 50)
        auction_data = InteractiveAuctionData(auction_id, False, True, 1,
                                              [first_lot])

        holding_an_auction.new_auction(auction_data)
        assert len(database_mock.auctions) == 1
        assert len(database_mock.lots) == 1
        auction = database_mock.auctions.get(auction_id)

        user_id = 25
        holding_an_auction.joining_an_auction(auction_id, user_id)
        holding_an_auction.joining_an_auction(auction_id, 2)
        assert len(participants_database_mock.participants) == 1
        assert len(participants_database_mock.participants[auction_id]) == 2

        mocked_end_start_auction = mocker.patch.object(holding_an_auction.timer, 'schedule_lot_finish')
        holding_an_auction.start_auction(auction_id)
        mocked_end_start_auction.assert_called_with(auction)

        with pytest.raises(ValueError) as e:
            holding_an_auction.agreement_with_bid(auction_id, user_id)
        assert str(e.value) == "Agreement is only for auction with lower bids"

        mocked_end_reschedule_lot_finish = mocker.patch.object(holding_an_auction.timer, 'reschedule_lot_finish')
        mocked_end_make_bid = mocker.patch.object(holding_an_auction.notifier, 'notify_new_bid')
        participants = participants_database_mock.get_auction_participants(auction_id)
        holding_an_auction.make_bid(auction_id, 2, 100)
        mocked_end_reschedule_lot_finish.assert_called_with(auction)
        mocked_end_make_bid.assert_called_with(participants, auction_id, 100, 2)
        last_price = 300
        holding_an_auction.make_bid(auction_id, user_id, last_price)
        mocked_end_make_bid.assert_called_with(participants, auction_id, last_price, user_id)

        finish_time_before_bid = datetime.datetime.now() + datetime.timedelta(seconds=10)
        auction.lots[auction.current_lot].finish_time = finish_time_before_bid

        auction.lots[auction.current_lot].finish_time = datetime.datetime.now()
        holding_an_auction.finish_lot(auction_id)
        assert database_mock.winners[first_lot_id] == (user_id, last_price)
