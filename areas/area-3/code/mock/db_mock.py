class DatabaseFunctionsMock:

    def __init__(self):
        self.auctions = {}
        self.lots = {}
        self.bids = {}
        self.winners = {}

    def save_information_about_the_winner(self, lot_id, user_id, price):
        self.winners[lot_id] = (user_id, price)

    def save_new_auction(self, auction):
        self.auctions[auction.get_id_auction()] = auction

    def load_auction(self, auction_id):
        return self.auctions[auction_id]

    def save_lots(self, lots):
        for lot in lots:
            self.lots[lot.lot_id] = lot

    def load_lot(self, lot_id):
        return self.lots[lot_id]

    def save_started_auction(self, auction):
        pass

    def update_bid(self, auction_id, user_id, price):
        self.bids[auction_id] = (user_id, price)

    def save_completed_lot(self, auction, current_lot):
        pass


class ParticipantDatabaseFunctionsMock:
    def __init__(self):
        self.participants = {}

    def add(self, auction_id, participant_id):
        current_participants = self.participants.get(auction_id, [])
        current_participants.append(participant_id)
        self.participants[auction_id] = current_participants

    def get_auction_participants(self, auction_id):
        return self.participants[auction_id]
